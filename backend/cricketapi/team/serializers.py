from rest_framework import serializers
from .models import Team


class TeamSerializer(serializers.ModelSerializer):

    total_points  = serializers.SerializerMethodField()


    class Meta:
        model = Team
        fields = '__all__'
        
    def get_total_points(self, obj):
        return 0

