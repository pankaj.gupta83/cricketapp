from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient


from team.models import Team 
from team.serializers import TeamSerializer


class GetAllTeamsTest(TestCase):

     
    def setUp(self):
        self.username = 'apitester'
        self.password = 'apitest@123'

        user = User(username=self.username, password=self.password)
        user.save()
        user = User.objects.get(username=self.username)
        #token = Token.objects.get(user__username=self.username)
        
        self.client = APIClient() 
        #self.client.credentials(HTTP_AUTHORIZATION='Authorization ' + token.key)
        self.client.force_authenticate(user=user)
        
        Team.objects.create(
            team_name='Team A', logo_url='http://cricket.sporting99.com/ipl/pics/mumbai-indians.jpg', 
            club_state='Delhi')
        Team.objects.create(
            team_name='Team B', logo_url='http://cricket.sporting99.com/ipl/pics/mumbai-indians.jpg', 
            club_state='Singapore')

    def test_get_all_teams(self):
        # get API response
        response = self.client.get('/api/v1/teams/')
        
        teams = Team.objects.all()
        serializer = TeamSerializer(teams, many=True)

        ## 200 Response 
        self.assertEqual(response.status_code, 200) 
        
        ## All teames List
        self.assertEqual(response.data, serializer.data)
