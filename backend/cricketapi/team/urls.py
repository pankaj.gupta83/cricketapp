from django.urls import include, path
from rest_framework import routers
from . import views
from django.conf.urls import include, url
from rest_framework.authtoken.views import obtain_auth_token


router = routers.DefaultRouter()
router.register(r'teams', views.TeamViewSet)


urlpatterns = [
    path('api/v1/', include(router.urls)),
]