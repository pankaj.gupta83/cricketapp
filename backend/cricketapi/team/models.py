from django.db import models
from datetime import datetime, timedelta, timezone, tzinfo

# Create your models here.

class Team(models.Model):
    id          = models.AutoField(primary_key=True)
    team_name   = models.CharField(max_length=60, unique = True)
    logo_url    = models.CharField(max_length=999)
    club_state  = models.CharField(max_length=100)
    
    class Meta:
        db_table = "team"
        indexes = [
            models.Index(fields=['club_state']),
            
        ]

    def __str__(self):
        return self.team_name

