from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated 
from rest_framework import viewsets
from rest_framework import generics
from django.core import serializers

from .models import Team
from .serializers import TeamSerializer

class TeamViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = Team.objects.all()
    serializer_class = TeamSerializer

    def get_queryset(self):
        team_id     = self.request.GET.get('team_id', None)

        queryset = Team.objects.all()

        if team_id:
            queryset = queryset.filter(id=team_id)

        return queryset

    
