from django.urls import include, path
from rest_framework import routers
from . import views
from django.conf.urls import include, url
from rest_framework.authtoken.views import obtain_auth_token


router = routers.DefaultRouter()


urlpatterns = [
    path('api/v1/', include(router.urls)),
    path('api/v1/api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/v1/api-token-auth/', obtain_auth_token, name='api_token_auth'),
]