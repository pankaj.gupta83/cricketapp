from django.db import models
from datetime import datetime, timedelta, timezone, tzinfo

class SysExtraType(models.Model):

    SYS_EXTRA_DEFAULT = 1

    id                      = models.AutoField(primary_key=True)
    entra_type_name         = models.CharField(max_length=255, unique=True)
    i_active                = models.BooleanField(
                                default=SYS_EXTRA_DEFAULT
                            )
    class Meta:
        db_table = "sys_extra_type"
                
    def __str__(self):
        return self.entra_type_name

