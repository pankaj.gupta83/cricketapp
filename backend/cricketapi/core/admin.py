from django.contrib import admin
from team.models import Team
from player.models import Player, TeamPlayer
from fixture.models import TeamMatch, MatchStat
from core.models import SysExtraType
# Register your models here.

admin.site.register(Team)
admin.site.register(Player)
admin.site.register(TeamPlayer)
admin.site.register(TeamMatch)
admin.site.register(MatchStat)
admin.site.register(SysExtraType)

