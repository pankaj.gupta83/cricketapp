from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated 
from rest_framework import viewsets
from rest_framework import generics
from django.core import serializers

from .models import SysExtraType
from .serializers import SysExtraTypeSerializer
# Create your views here.

class SysExtraTypeViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = SysExtraType.objects.all()
    serializer_class = SysExtraTypeSerializer

    def get_queryset(self):
        queryset = SysExtraType.objects.all()
        return queryset            
