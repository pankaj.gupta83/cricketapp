from rest_framework import serializers
from .models import SysExtraType

class SysExtraTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = SysExtraType
        fields = '__all__'



