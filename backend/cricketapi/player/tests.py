from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
import json
from team.models import Team
from team.serializers import TeamSerializer
from player.models import Player , TeamPlayer
from player.serializers import PlayerSerializer, TeamPlayerSerializer


class GetAllPlayerTest(TestCase):

    def setUp(self):
        self.username = 'apitester'
        self.password = 'apitest@123'

        user = User(username=self.username, password=self.password)
        user.save()
        user = User.objects.get(username=self.username)
        #token = Token.objects.get(user__username=self.username)
        
        self.client = APIClient() 
        #self.client.credentials(HTTP_AUTHORIZATION='Authorization ' + token.key)
        self.client.force_authenticate(user=user)
        
        Team.objects.create(
            team_name='Team A', logo_url='http://cricket.sporting99.com/ipl/pics/mumbai-indians.jpg', 
            club_state='Delhi')
        Team.objects.create(
            team_name='Team B', logo_url='http://cricket.sporting99.com/ipl/pics/mumbai-indians.jpg', 
            club_state='Singapore')

        Player.objects.create(
            first_name='Player A First',last_name='Player A Last', image_uri='http://cricket.sporting99.com/ipl/pics/mumbai-indians.jpg', 
            jersey_number=88,country_id = 1)
        Player.objects.create(
            first_name='Player B First',last_name='Player B Last', image_uri='http://cricket.sporting99.com/ipl/pics/mumbai-indians.jpg', 
            jersey_number=99,country_id = 1)

        TeamPlayer.objects.create(
            contract_start_date='2019-01-01', contract_end_date='2019-12-31', is_active=1, 
            player_id=Player.objects.get(jersey_number=88), team_id=Team.objects.get(club_state='Delhi')
        )

        TeamPlayer.objects.create(
            contract_start_date='2019-01-01', contract_end_date='2019-12-31', is_active=1, 
            player_id=Player.objects.get(jersey_number=99), team_id=Team.objects.get(club_state='Singapore')
        )

    def test_get_all_players(self):
        # get API response
        response = self.client.get('/api/v1/players/')
        
        playerList = Player.objects.all()
        serializer = PlayerSerializer(playerList, many=True)

        ## 200 Response 
        self.assertEqual(response.status_code, 200) 
        
        ## All teames List
        self.assertEqual(response.data, serializer.data)

    def test_get_all_teams(self):
        # get API response
        response = self.client.get('/api/v1/teams/')
        
        teamList = Team.objects.all()
        serializer = TeamSerializer(teamList, many=True)

        ## 200 Response 
        self.assertEqual(response.status_code, 200) 
        
        ## All teames List
        self.assertEqual(response.data, serializer.data)

    def test_get_team_players(self):
        # get API response
        teamList = Team.objects.values('id').get(club_state='Singapore')
        teamId = teamList.get('id')
        teamId = str(teamId)
        
        apiResponse = self.client.get('/api/v1/teamplayers/?team_id=' + teamId)
        dbResponse  = TeamPlayer.objects.all().get(team_id=teamId)
        serializerDB = TeamPlayerSerializer(dbResponse)
        
        if isinstance (serializerDB.data, list) == False:
            serializeList = [serializerDB.data] 
        
        ## 200 Response 
        self.assertEqual(apiResponse.status_code, 200) 
        
        ## Data Comparison
        self.assertEqual(apiResponse.data, serializeList)
        