from rest_framework import serializers
from .models import Player, TeamPlayer
from team.serializers import TeamSerializer


class PlayerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Player
        fields = '__all__'


class TeamPlayerSerializer(serializers.ModelSerializer):

    team_id   = TeamSerializer()
    player_id = PlayerSerializer()

    class Meta:
        model = TeamPlayer
        fields = ['id', 'team_id', 'player_id', 'contract_start_date','contract_end_date','is_active']
