from django.db import models
from datetime import datetime, timedelta, timezone, tzinfo
from team.models import Team

class Player(models.Model):

    id              = models.AutoField(primary_key=True)
    first_name      = models.CharField(max_length=60)
    last_name       = models.CharField(max_length=60)
    image_uri       = models.CharField(max_length=999)
    jersey_number   = models.IntegerField()
    country_id      = models.IntegerField()
    
    class Meta:
        db_table = "player"
        indexes = [
            models.Index(fields=['first_name']),
            models.Index(fields=['last_name']),
        ]

    def __str__(self):
        return " ".join([self.first_name, self.last_name])


class TeamPlayer(models.Model):

    id                      = models.AutoField(primary_key=True)
    team_id                 = models.ForeignKey(
                            Team,
                            related_name='team_id',
                            db_column='team_id',
                            on_delete=models.SET_NULL,
                            null=True, 
                        )
    player_id              = models.ForeignKey(
                            Player,
                            related_name='player_id',
                            db_column='player_id',
                            on_delete=models.SET_NULL,
                            null=True,
                        )
    contract_start_date = models.DateField()
    contract_end_date   = models.DateField()
    is_active           = models.BooleanField(
                            #default=1
                        )

    class Meta:
        db_table = "team_player"
        unique_together = ('team_id', 'player_id',)
        
    def __str__(self):
        return str(self.player_id)
        #return self.image_uri
            
        #playerName = models.OneToOneField(Player)
    
        #playerN = ", ".join([playerName.first_name, playerName.last_name])
        #print(playerN)
        #return ", ".join([playerName.first_name, playerName.last_name])
        
