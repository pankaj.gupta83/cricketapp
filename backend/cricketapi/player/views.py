from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated 
from rest_framework import viewsets
from rest_framework import generics
from django.core import serializers

from .models import Player, TeamPlayer
from .serializers import PlayerSerializer, TeamPlayerSerializer

class PlayerViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer

    def get_queryset(self):
        queryset = Player.objects.all()
        return queryset


class TeamPlayerViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = TeamPlayer.objects.all()
    serializer_class = TeamPlayerSerializer
    
    def get_queryset(self):
        queryset    = TeamPlayer.objects.all()
        team_id     = self.request.GET.get('team_id', None)

        if team_id:
            queryset = TeamPlayer.objects.all().filter(team_id=team_id)

        return queryset
    
