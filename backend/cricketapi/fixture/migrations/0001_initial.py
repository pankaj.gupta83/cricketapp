# Generated by Django 2.2.7 on 2019-11-21 09:00

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('team', '0001_initial'),
        ('player', '0001_initial'),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='VwMatchStat',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('team_match_id', models.IntegerField()),
                ('team_player_id', models.IntegerField()),
                ('team_id', models.IntegerField()),
                ('player_id', models.IntegerField()),
                ('first_name', models.CharField(max_length=255)),
                ('last_name', models.CharField(max_length=255)),
                ('image_uri', models.CharField(max_length=255)),
                ('jersey_number', models.IntegerField()),
                ('country_id', models.IntegerField()),
                ('team_name', models.CharField(max_length=255)),
                ('logo_url', models.CharField(max_length=255)),
                ('club_state', models.CharField(max_length=255)),
                ('batting_order', models.IntegerField()),
                ('score_run', models.IntegerField()),
                ('score_wicket', models.IntegerField()),
                ('score_catch', models.IntegerField()),
                ('score_runout', models.IntegerField()),
                ('inning_start_datetime', models.DateTimeField(default=datetime.datetime.now)),
                ('inning_end_datetime', models.DateTimeField()),
                ('is_mom', models.BooleanField()),
                ('is_any_violation', models.IntegerField()),
                ('violation_id', models.IntegerField()),
            ],
            options={
                'db_table': 'vw_match_stat',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='VwTeamPoint',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('team_name', models.CharField(max_length=255)),
                ('logo_url', models.CharField(max_length=999)),
                ('club_state', models.CharField(max_length=255)),
                ('earned_points', models.IntegerField()),
            ],
            options={
                'db_table': 'vw_team_point',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='TeamMatch',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('match_type', models.CharField(choices=[('TM', 'Test'), ('OD', 'Onedayer'), ('T2', 'T20')], default='OD', max_length=2)),
                ('venue', models.CharField(max_length=999)),
                ('match_start_date', models.DateField()),
                ('match_end_date', models.DateField()),
                ('a_team_point', models.IntegerField()),
                ('b_team_point', models.IntegerField()),
                ('toss_winner_choice', models.CharField(choices=[('BAT', 'Bating'), ('BOW', 'Bowling')], max_length=3)),
                ('match_result', models.CharField(choices=[('DR', 'Draw'), ('TI', 'Tie'), ('DC', 'Declared'), ('CA', 'Cancelled')], default='DC', max_length=2)),
                ('win_margin', models.CharField(max_length=999)),
                ('a_team_id', models.ForeignKey(db_column='a_team_id', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='a_team_id', to='team.Team')),
                ('b_team_id', models.ForeignKey(db_column='b_team_id', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='b_team_id', to='team.Team')),
                ('toss_winner_team_id', models.ForeignKey(db_column='toss_winner_team_id', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='toss_winner_team_id', to='team.Team')),
                ('winner_team_id', models.ForeignKey(db_column='winner_team_id', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='winner_team_id', to='team.Team')),
            ],
            options={
                'db_table': 'team_match',
            },
        ),
        migrations.CreateModel(
            name='MatchStatExtra',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('score_extra', models.IntegerField()),
                ('sys_extra_type_id', models.ForeignKey(db_column='sys_extra_type_id', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='match_stat_extra_sys_extra_type_id', to='core.SysExtraType')),
                ('team_match_id', models.ForeignKey(db_column='team_match_id', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='match_stat_extra_team_match_id', to='fixture.TeamMatch')),
            ],
            options={
                'db_table': 'match_stat_extra',
            },
        ),
        migrations.CreateModel(
            name='MatchStat',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('batting_order', models.IntegerField()),
                ('score_run', models.IntegerField()),
                ('score_wicket', models.IntegerField()),
                ('score_catch', models.IntegerField()),
                ('score_runout', models.IntegerField()),
                ('inning_start_datetime', models.DateTimeField(default=datetime.datetime.now)),
                ('inning_end_datetime', models.DateTimeField(default=datetime.datetime.now)),
                ('is_mom', models.BooleanField()),
                ('is_any_violation', models.IntegerField()),
                ('violation_id', models.IntegerField(null=True)),
                ('team_match_id', models.ForeignKey(db_column='team_match_id', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='team_match_id', to='fixture.TeamMatch')),
                ('team_player_id', models.ForeignKey(db_column='team_player_id', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='team_player_id', to='player.TeamPlayer')),
            ],
            options={
                'db_table': 'match_stat',
            },
        ),
        migrations.AddIndex(
            model_name='teammatch',
            index=models.Index(fields=['match_type'], name='team_match_match_t_ae38f9_idx'),
        ),
        migrations.AlterUniqueTogether(
            name='matchstatextra',
            unique_together={('sys_extra_type_id', 'team_match_id')},
        ),
        migrations.AlterUniqueTogether(
            name='matchstat',
            unique_together={('team_match_id', 'team_player_id')},
        ),
    ]
