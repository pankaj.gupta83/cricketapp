from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
import json
from team.models import Team
from team.serializers import TeamSerializer
from django.db.models import Q
from django.db.models import Count, Sum
import datetime
from django.utils import timezone
from django.db.models.query import QuerySet

from player.models import Player , TeamPlayer
from player.serializers import PlayerSerializer, TeamPlayerSerializer
from fixture.models import TeamMatch, MatchStat, VwMatchStat, VwTeamPoint
from fixture.serializers import TeamMatchSerializer, MatchStatSerializer, VwMatchStatSerializer, VwTeamPointSerializer


class GetTeamMatchTest(TestCase):

    def setUp(self):
        self.username = 'apitester'
        self.password = 'apitest@123'

        user = User(username=self.username, password=self.password)
        user.save()
        user = User.objects.get(username=self.username)
        #token = Token.objects.get(user__username=self.username)
        
        self.client = APIClient() 
        #self.client.credentials(HTTP_AUTHORIZATION='Authorization ' + token.key)
        self.client.force_authenticate(user=user)
        
        Team.objects.create(
            team_name='Team A', logo_url='http://cricket.sporting99.com/ipl/pics/mumbai-indians.jpg', 
            club_state='Delhi')
        Team.objects.create(
            team_name='Team B', logo_url='http://cricket.sporting99.com/ipl/pics/mumbai-indians.jpg', 
            club_state='Singapore')
        Team.objects.create(
            team_name='Team C', logo_url='http://cricket.sporting99.com/ipl/pics/mumbai-indians.jpg', 
            club_state='Mumbai')
            
        Player.objects.create(
            first_name='Player A First',last_name='Player A Last', image_uri='http://cricket.sporting99.com/ipl/pics/mumbai-indians.jpg', 
            jersey_number=88,country_id = 1)
        Player.objects.create(
            first_name='Player B First',last_name='Player B Last', image_uri='http://cricket.sporting99.com/ipl/pics/mumbai-indians.jpg', 
            jersey_number=99,country_id = 1)
        
        TeamPlayer.objects.create(
            contract_start_date='2019-01-01', contract_end_date='2019-12-31', is_active=1, 
            player_id=Player.objects.get(jersey_number=88), team_id=Team.objects.get(club_state='Delhi')
        )

        TeamPlayer.objects.create(
            contract_start_date='2019-01-01', contract_end_date='2019-12-31', is_active=1, 
            player_id=Player.objects.get(jersey_number=99), team_id=Team.objects.get(club_state='Singapore')
        )
        
        TeamMatch.objects.create(
            match_type='T2', venue='Delhi', match_start_date='2019-01-01', 
            match_end_date='2019-01-01', a_team_point=2, b_team_point=0, 
            toss_winner_choice='BAT', match_result='DC', win_margin='10', 
            a_team_id=Team.objects.get(club_state='Singapore'), b_team_id=Team.objects.get(club_state='Singapore'), 
            toss_winner_team_id=Team.objects.get(club_state='Singapore'), winner_team_id=Team.objects.get(club_state='Singapore'),
        )
        
        TeamMatch.objects.create(
            match_type='T2', venue='Mumbai', match_start_date='2019-01-02', 
            match_end_date='2019-01-02', a_team_point=2, b_team_point=0, 
            toss_winner_choice='BAT', match_result='DC', win_margin='20', 
            a_team_id=Team.objects.get(club_state='Singapore'), b_team_id=Team.objects.get(club_state='Mumbai'), 
            toss_winner_team_id=Team.objects.get(club_state='Singapore'), winner_team_id=Team.objects.get(club_state='Singapore'),
        )
        
        MatchStat.objects.create(
            batting_order=1, score_run=79, score_wicket=3, 
            score_catch=1, score_runout=2, inning_start_datetime=timezone.now(), 
            inning_end_datetime=timezone.now(), is_mom=1, is_any_violation=0, 
            violation_id=0, team_match_id=TeamMatch.objects.get(venue='Mumbai'), 
            team_player_id=TeamPlayer.objects.get(player_id=Player.objects.get(jersey_number=99))
        )
        
        
    def test_get_all_fixtures(self):
        # get API response
        apiResponse = self.client.get('/api/v1/teammatches/')
        
        dbMatchList = TeamMatch.objects.all()
        dbSerializer = TeamMatchSerializer(dbMatchList, many=True)

        ## 200 Response 
        self.assertEqual(apiResponse.status_code, 200) 
        
        ## All teams List
        self.assertEqual(apiResponse.data, dbSerializer.data)


    def test_get_team_fixtures(self):

        teamList = Team.objects.values('id').get(club_state='Singapore')
        teamId = teamList.get('id')
        teamId = str(teamId)
       
        # get API response
        apiResponse = self.client.get('/api/v1/teammatches/?team_id=' + teamId)
        
        dbMatchList = TeamMatch.objects.all().filter(Q(a_team_id=teamId) | Q(b_team_id=teamId))
        dbMatchList = dbMatchList.annotate(points_earned=Sum('a_team_point')).order_by()

        dbSerializer = TeamMatchSerializer(dbMatchList, many=True)

        ## 200 Response 
        self.assertEqual(apiResponse.status_code, 200) 
        
        ## All teams List
        self.assertEqual(apiResponse.data, dbSerializer.data)
    
    #/matchdetails/player_id=2
    def test_get_player_stats(self):

        playerList    = Player.objects.values('id').get(jersey_number=99)
        playerId      = playerList.get('id')
        playerId      = str(playerId)
        
        # get API response
        apiResponse = self.client.get('/api/v1/matchdetails/?player_id=' + playerId)
        
        #dbResponse      = VwMatchStat.objects.get(player_id=playerId)
        rawSql = 'select *, SUM(score_run) as total_run, SUM(score_wicket) as total_wicket, \
        SUM(score_catch) as total_catch, SUM(score_runout) as total_runout,\
        COUNT(team_match_id) AS total_matches, MAX(score_run) AS highest_score,\
        SUM(IF(score_run >= 50 AND score_run <=99, 1, 0)) AS total_fifty,\
        SUM(IF(score_run >= 100, 1, 0)) AS total_hundred\
        FROM vw_match_stat where  team_id IS NOT NULL '
        rawSql += ' AND player_id=' + playerId
        rawSql += ' GROUP BY player_id'
        
        dbResponse        = VwMatchStat.objects.raw(rawSql)
        #dbResponse      = queryset.raw(rawSql)
        dbSerializer    = VwMatchStatSerializer(dbResponse, many=True)
        #print(json.dumps(apiResponse.data))
        #print(json.dumps(dbSerializer.data))
        ## 200 Response 
        self.assertEqual(apiResponse.status_code, 200) 
        ## All teams List
        self.assertEqual(apiResponse.data, dbSerializer.data)


    def test_get_team_points(self):

        #playerList    = Player.objects.values('id').get(jersey_number=99)
        #playerId      = playerList.get('id')
        #playerId      = str(playerId)
        
        # get API response
        apiResponse = self.client.get('/api/v1/teampoints/')
        
        dbResponse      = VwTeamPoint.objects.all().order_by('-earned_points', 'team_name')
        dbSerializer    = VwTeamPointSerializer(dbResponse, many=True)
        #print(json.dumps(apiResponse.data))
        #print(json.dumps(dbSerializer.data))
        ## 200 Response 
        self.assertEqual(apiResponse.status_code, 200) 
        ## All teams List
        self.assertEqual(apiResponse.data, dbSerializer.data)



