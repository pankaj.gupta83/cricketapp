from django.urls import include, path
from rest_framework import routers
from . import views
from django.conf.urls import include, url
from rest_framework.authtoken.views import obtain_auth_token


router = routers.DefaultRouter()
router.register(r'teammatches', views.TeamMatchViewSet)
router.register(r'matchstats', views.MatchStatViewSet)
router.register(r'matchdetails', views.VwMatchStatViewSet)
router.register(r'teampoints', views.VwTeamPointViewSet)
router.register(r'matchstatextras', views.MatchStatExtraViewSet)

urlpatterns = [
    path('api/v1/', include(router.urls)),
]