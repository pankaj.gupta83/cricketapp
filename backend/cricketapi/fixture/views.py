from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated 
from rest_framework import viewsets
from rest_framework import generics
from django.core import serializers
from django.db.models import Q
from django.db.models import Count, Sum
#from django.db import connection

from .models import TeamMatch, MatchStat, VwMatchStat, VwTeamPoint, MatchStatExtra
from .serializers import TeamMatchSerializer, MatchStatSerializer, VwMatchStatSerializer, VwTeamPointSerializer, MatchStatExtraSerializer
# Create your views here.

class TeamMatchViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = TeamMatch.objects.all()
    serializer_class = TeamMatchSerializer

    def get_queryset(self):
        queryset = TeamMatch.objects.all()

        team_id         = self.request.GET.get('team_id', None)
        team_match_id   = self.request.GET.get('match_id', None)
        
        if team_id:
            queryset = TeamMatch.objects.all().filter(Q(a_team_id=team_id) | Q(b_team_id=team_id))
           
        queryset = queryset.annotate(points_earned=Sum('a_team_point')).order_by()

        return queryset
       

class MatchStatViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = MatchStat.objects.all()
    serializer_class = MatchStatSerializer

    def get_queryset(self):
        queryset = MatchStat.objects

        team_id                 = self.request.GET.get('team_id', None)
        team_match_id           = self.request.GET.get('match_id', None)
        team_player_id          = self.request.GET.get('team_player_id', None)
        player_id               = self.request.GET.get('player_id', None)

        queryset = MatchStat.objects.all()

        return queryset                


class VwMatchStatViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = VwMatchStat.objects.all()
    serializer_class = VwMatchStatSerializer

    def get_queryset(self):
        queryset = VwMatchStat.objects

        team_id                 = self.request.GET.get('team_id', None)
        team_match_id           = self.request.GET.get('match_id', None)
        team_player_id          = self.request.GET.get('team_player_id', None)
        player_id               = self.request.GET.get('player_id', None)

        #queryset = MatchStat.objects.all()

        rawSql = 'SELECT *, SUM(score_run) as total_run, SUM(score_wicket) as total_wicket,\
        SUM(score_catch) as total_catch, SUM(score_runout) as total_runout,\
        COUNT(team_match_id) AS total_matches, MAX(score_run) AS highest_score,\
        SUM(IF(score_run >= 50 AND score_run <=99, 1, 0)) AS total_fifty,\
        SUM(IF(score_run >= 100, 1, 0)) AS total_hundred\
        FROM vw_match_stat where team_id IS NOT NULL '
        
        if team_match_id:
             rawSql += ' AND team_match_id=' + team_match_id
             rawSql += ' GROUP BY team_match_id, player_id '
            
        elif team_player_id:
            rawSql += ' AND team_player_id=' + team_player_id
            rawSql += ' GROUP BY team_player_id'
            
        elif player_id:
            rawSql += ' AND player_id=' + player_id
            rawSql += ' GROUP BY player_id'
        else:
            rawSql += ' GROUP BY player_id'
            
        rawSql += ' ORDER BY team_id ASC, first_name ASC'    
        #print(rawSql)
        queryset = queryset.raw(rawSql)

        return queryset                


class VwTeamPointViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = VwTeamPoint.objects.all()
    serializer_class = VwTeamPointSerializer

    def get_queryset(self):

        queryset        = VwTeamPoint.objects.all()
        team_id         = self.request.GET.get('team_id', None)
        
        if team_id:
            queryset = queryset.filter(id=team_id)

        queryset = queryset.order_by('-earned_points', 'team_name')

        return queryset                


class MatchStatExtraViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = MatchStatExtra.objects.all()
    serializer_class = MatchStatExtraSerializer

    def get_queryset(self):
        queryset = MatchStatExtra.objects.all()

        team_match_id   = self.request.GET.get('match_id', None)
        
        if team_match_id:
            queryset = MatchStatExtra.objects.filter(team_match_id=team_match_id) 
        
        queryset = queryset.filter(sys_extra_type_id__i_active=True) 
        queryset = queryset.order_by('team_id', 'sys_extra_type_id')
        return queryset
       