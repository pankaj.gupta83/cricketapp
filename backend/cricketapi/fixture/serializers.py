from rest_framework import serializers
from .models import TeamMatch, MatchStat, VwMatchStat, VwTeamPoint, MatchStatExtra
from core.models import SysExtraType
from team.serializers import TeamSerializer
from player.serializers import TeamPlayerSerializer
from core.serializers import SysExtraTypeSerializer

class TeamMatchSerializer(serializers.ModelSerializer):

    a_team_id               = TeamSerializer()
    b_team_id               = TeamSerializer()
    toss_winner_team_id     = TeamSerializer()
    winner_team_id          = TeamSerializer()
    
    ##Custom fields added into response
    #points_earned   = serializers.SerializerMethodField()
    margin_type     = serializers.SerializerMethodField()
    result_details  = serializers.SerializerMethodField()

    class Meta:
        model = TeamMatch
        fields = '__all__'

    def get_points_earned(self, obj):
        return obj.points_earned

    def get_margin_type(self, obj):
        margin_type = 'Wickets'

        if(obj.winner_team_id == obj.toss_winner_team_id and obj.toss_winner_choice == 'BAT'):
            margin_type = 'Runs'

        obj.margin_type = margin_type
        return obj.margin_type
        
    def get_result_details(self, obj):

        if (int)(obj.win_margin) > 0:
            result = 'Match winner was ' + obj.winner_team_id.team_name + ' by Margin of ' +  obj.win_margin + ' ' + obj.margin_type
        else:
            result = 'Match Tie!'        

        obj.result = result
        return result


class MatchStatExtraSerializer(serializers.ModelSerializer):

    team_id = TeamSerializer()
    sys_extra_type_id = SysExtraTypeSerializer()
    
    class Meta:
        model = MatchStatExtra
        fields = '__all__'


class MatchStatSerializer(serializers.ModelSerializer):

    team_player_id  = TeamPlayerSerializer()
    team_match_id   = TeamMatchSerializer()
    team_player_id  = serializers.StringRelatedField(read_only=True)
    
    class Meta:
        model = MatchStat
        fields  = '__all__'


class VwMatchStatSerializer(serializers.ModelSerializer):

    total_run                   = serializers.IntegerField()
    total_wicket                = serializers.IntegerField()
    total_catch                 = serializers.IntegerField()
    total_runout                = serializers.IntegerField()
    total_matches               = serializers.IntegerField()
    highest_score               = serializers.IntegerField()
    total_fifty                 = serializers.IntegerField()
    total_hundred               = serializers.IntegerField()
    
    class Meta:
        model = VwMatchStat
        fields  = '__all__'

    

class VwTeamPointSerializer(serializers.ModelSerializer):

    class Meta:
        model = VwTeamPoint
        fields  = '__all__'



