from django.db import models
from datetime import datetime, timedelta, timezone, tzinfo
from team.models import Team
from core.models import SysExtraType
from player.models import TeamPlayer

class TeamMatch(models.Model):

    MATCH_TYPE_CHOICES = [
        ('TM', 'Test'),
        ('OD', 'Onedayer'),
        ('T2', 'T20'),
    ]
    DEFAULT_MATCH_TYPE_CHOICES = 'OD'

    TOSS_TYPE_CHOICES = [
        ('BAT', 'Bating'),
        ('BOW', 'Bowling'),
    ]

    MATCH_RESULT_TYPE_CHOICES = [
        ('DR', 'Draw'),
        ('TI', 'Tie'),
        ('DC', 'Declared'),
        ('CA', 'Cancelled'),
    ]

    DEFAULT_MATCH_RESULT_TYPE_CHOICES = 'DC'
    
    id                  = models.AutoField(primary_key=True)
    match_type          = models.CharField(
                            max_length=2,
                            choices=MATCH_TYPE_CHOICES,
                            default=DEFAULT_MATCH_TYPE_CHOICES,
                        )
    a_team_id           = models.ForeignKey(
                            Team,
                            related_name='a_team_id',
                            db_column='a_team_id',
                            on_delete=models.SET_NULL,
                            null=True,
                        )
    b_team_id           = models.ForeignKey(
                            Team,
                            related_name='b_team_id',
                            db_column='b_team_id',
                            on_delete=models.SET_NULL,
                            null=True,
                        )
                            
    venue               = models.CharField(max_length=999)
    match_start_date    = models.DateField()
    match_end_date      = models.DateField()
    a_team_point        = models.IntegerField()
    b_team_point        = models.IntegerField()
    toss_winner_choice  = models.CharField(
                            max_length=3,
                            choices=TOSS_TYPE_CHOICES,
                        )
    toss_winner_team_id = models.ForeignKey(
                            Team,
                            related_name='toss_winner_team_id',
                            db_column='toss_winner_team_id',
                            on_delete=models.SET_NULL,
                            null=True,
                        )                            
    match_result        = models.CharField(
                            max_length=2,
                            choices=MATCH_RESULT_TYPE_CHOICES,
                            default=DEFAULT_MATCH_RESULT_TYPE_CHOICES,
                        )
    winner_team_id      = models.ForeignKey(
                            Team,
                            related_name='winner_team_id',
                            db_column='winner_team_id',
                            on_delete=models.SET_NULL,
                            null=True,
                        )                        
                    
    win_margin          = models.CharField(max_length=999)

    class Meta:
        db_table = "team_match"
        indexes = [
            models.Index(fields=['match_type']),
        ]
    
    def __str__(self):
        #return self.match_id
        return " vs ".join([str(self.a_team_id), str(self.b_team_id)])


class MatchStatExtra(models.Model):

    id                  = models.AutoField(primary_key=True)
    team_match_id       = models.ForeignKey(
                            TeamMatch,
                            related_name='match_stat_extra_team_match_id',
                            db_column='team_match_id',
                            on_delete=models.SET_NULL,
                            null=True,
                        )
    sys_extra_type_id   = models.ForeignKey(
                            SysExtraType,
                            related_name='match_stat_extra_sys_extra_type_id',
                            db_column='sys_extra_type_id',
                            on_delete=models.SET_NULL,
                            null=True,
                        )
    score_extra         = models.IntegerField()

    team_id             = models.ForeignKey(
                            Team,
                            related_name='match_stat_extra_team_id',
                            db_column='team_id',
                            on_delete=models.SET_NULL,
                            null=True,
                        )
    
    
    class Meta:
        db_table = "match_stat_extra"
        unique_together = ('sys_extra_type_id', 'team_match_id', 'team_id')
        
    def __int__(self):
        return self.id


class MatchStat(models.Model):

    id                      = models.AutoField(primary_key=True)
    team_match_id           = models.ForeignKey(
                                TeamMatch,
                                related_name='team_match_id',
                                db_column='team_match_id',
                                on_delete=models.SET_NULL,
                                null=True,
                            )
    team_player_id          = models.ForeignKey(
                                TeamPlayer,
                                related_name='team_player_id',
                                db_column='team_player_id',
                                on_delete=models.SET_NULL,
                                null=True,
                            )
    batting_order           = models.IntegerField()
    score_run               = models.IntegerField()
    score_wicket            = models.IntegerField()
    score_catch             = models.IntegerField()
    score_runout            = models.IntegerField()
    inning_start_datetime   = models.DateTimeField(default=datetime.now)
    inning_end_datetime     = models.DateTimeField(default=datetime.now)
    is_mom                  = models.BooleanField()
    is_any_violation        = models.IntegerField()

    # A separate violation rules table need to create
    violation_id            = models.IntegerField(
                                null=True
                            )

    class Meta:
        db_table = "match_stat"
        unique_together = ('team_match_id', 'team_player_id',)
        
    def __str__(self):
        return " - ".join([str(self.team_match_id) , str(self.team_player_id)])


class VwMatchStat(models.Model):

    id              = models.AutoField(primary_key=True)

    team_match_id       = models.IntegerField()
    team_player_id      = models.IntegerField()
    team_id             = models.IntegerField()
    player_id       = models.IntegerField()
    first_name      = models.CharField(max_length=255)
    last_name       = models.CharField(max_length=255)
    image_uri       = models.CharField(max_length=255)
    jersey_number   = models.IntegerField()
    country_id      = models.IntegerField()
  
    team_name  = models.CharField(max_length=255)
    logo_url   = models.CharField(max_length=255)
    club_state   = models.CharField(max_length=255)

  
    batting_order           = models.IntegerField()
    score_run               = models.IntegerField()
    score_wicket            = models.IntegerField()
    score_catch             = models.IntegerField()
    score_runout            = models.IntegerField()
    inning_start_datetime   = models.DateTimeField(default=datetime.now)
    inning_end_datetime     = models.DateTimeField()
    is_mom                  = models.BooleanField()
    is_any_violation        = models.IntegerField()
    violation_id            = models.IntegerField()
  
    
    class Meta:
        db_table    = "vw_match_stat"
        managed     = False
        
    def __str__(self):
            return self.player_id


class VwTeamPoint(models.Model):

    id              = models.AutoField(primary_key=True)
    team_name       = models.CharField(max_length=255)
    logo_url        = models.CharField(max_length=999)
    club_state      = models.CharField(max_length=255)
    earned_points   = models.IntegerField()
    total_match     = models.IntegerField()
    win_match       = models.IntegerField()
    lost_match      = models.IntegerField()
    tied_match      = models.IntegerField()
        
    class Meta:
        db_table    = "vw_team_point"
        managed     = False
        
    def __str__(self):
            return self.team_name
        