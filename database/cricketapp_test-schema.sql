DELIMITER $$

DROP FUNCTION IF EXISTS `get_earned_points`$$

CREATE FUNCTION `get_earned_points`(p_team_match_id INT(11), p_team_id INT(11)) RETURNS INT(11)
    DETERMINISTIC
BEGIN 
	DECLARE v_points INTEGER(4);
	SELECT (IF(a_team_id = p_team_id, a_team_point, 0) + IF(b_team_id = p_team_id, b_team_point, 0)) INTO v_points 
	FROM team_match 
	WHERE ((a_team_id = p_team_id OR b_team_id = p_team_id)) AND id = p_team_match_id;
	
	RETURN IFNULL(v_points, 0);
END$$

DELIMITER ;

DELIMITER $$
CREATE VIEW `vw_team_point` AS 
SELECT
  `team`.`id`         AS `id`,
  `team`.`team_name`  AS `team_name`,
  `team`.`logo_url`   AS `logo_url`,
  `team`.`club_state` AS `club_state`,
  COUNT(`team_match`.`id`) AS `total_match`,
  SUM(IF(((`team_match`.`winner_team_id` = `team`.`id`) AND (`team_match`.`match_result` = 'DC')),1,0)) AS `win_match`,
  SUM(IF(((`team_match`.`winner_team_id` <> `team`.`id`) AND (`team_match`.`match_result` = 'DC')),1,0)) AS `lost_match`,
  SUM(IF((`team_match`.`match_result` = 'TI'),1,0)) AS `tied_match`,
  SUM(`get_earned_points`(`team_match`.`id`,`team`.`id`)) AS `earned_points`
FROM (`team`
   LEFT JOIN `team_match`
     ON (((`team`.`id` = `team_match`.`a_team_id`)
           OR (`team`.`id` = `team_match`.`b_team_id`))))
GROUP BY `team`.`id`$$

DELIMITER ;

DELIMITER $$
CREATE VIEW `vw_match_stat` AS 
SELECT
  `player`.`id`                        AS `player_id`,
  `player`.`first_name`                AS `first_name`,
  `player`.`last_name`                 AS `last_name`,
  `player`.`image_uri`                 AS `image_uri`,
  `player`.`jersey_number`             AS `jersey_number`,
  `player`.`country_id`                AS `country_id`,
  `team`.`id`                          AS `team_id`,
  `team`.`team_name`                   AS `team_name`,
  `team`.`logo_url`                    AS `logo_url`,
  `team`.`club_state`                  AS `club_state`,
  `match_stat`.`id`                    AS `id`,
  `match_stat`.`batting_order`         AS `batting_order`,
  `match_stat`.`score_run`             AS `score_run`,
  `match_stat`.`score_wicket`          AS `score_wicket`,
  `match_stat`.`score_catch`           AS `score_catch`,
  `match_stat`.`score_runout`          AS `score_runout`,
  `match_stat`.`inning_start_datetime` AS `inning_start_datetime`,
  `match_stat`.`inning_end_datetime`   AS `inning_end_datetime`,
  `match_stat`.`is_mom`                AS `is_mom`,
  `match_stat`.`is_any_violation`      AS `is_any_violation`,
  `match_stat`.`violation_id`          AS `violation_id`,
  `match_stat`.`team_player_id`        AS `team_player_id`,
  `match_stat`.`team_match_id`         AS `team_match_id`
FROM ((((`match_stat`
      LEFT JOIN `team_player`
        ON ((`match_stat`.`team_player_id` = `team_player`.`id`)))
     LEFT JOIN `team_match`
       ON ((`match_stat`.`team_match_id` = `team_match`.`id`)))
    LEFT JOIN `team`
      ON ((`team_player`.`team_id` = `team`.`id`)))
   LEFT JOIN `player`
     ON ((`team_player`.`player_id` = `player`.`id`)))$$

DELIMITER ;