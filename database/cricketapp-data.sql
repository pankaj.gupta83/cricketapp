/*
SQLyog Community
MySQL - 5.7.26 : Database - cricketapp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Data for the table `auth_group` */

/*Data for the table `auth_group_permissions` */

/*Data for the table `auth_permission` */

insert  into `auth_permission`(`id`,`name`,`content_type_id`,`codename`) values 
(1,'Can add player',1,'add_player'),
(2,'Can change player',1,'change_player'),
(3,'Can delete player',1,'delete_player'),
(4,'Can view player',1,'view_player'),
(5,'Can add sys extra type',2,'add_sysextratype'),
(6,'Can change sys extra type',2,'change_sysextratype'),
(7,'Can delete sys extra type',2,'delete_sysextratype'),
(8,'Can view sys extra type',2,'view_sysextratype'),
(9,'Can add team',3,'add_team'),
(10,'Can change team',3,'change_team'),
(11,'Can delete team',3,'delete_team'),
(12,'Can view team',3,'view_team'),
(13,'Can add team player',4,'add_teamplayer'),
(14,'Can change team player',4,'change_teamplayer'),
(15,'Can delete team player',4,'delete_teamplayer'),
(16,'Can view team player',4,'view_teamplayer'),
(17,'Can add team match',5,'add_teammatch'),
(18,'Can change team match',5,'change_teammatch'),
(19,'Can delete team match',5,'delete_teammatch'),
(20,'Can view team match',5,'view_teammatch'),
(21,'Can add match stat extra',6,'add_matchstatextra'),
(22,'Can change match stat extra',6,'change_matchstatextra'),
(23,'Can delete match stat extra',6,'delete_matchstatextra'),
(24,'Can view match stat extra',6,'view_matchstatextra'),
(25,'Can add match stat',7,'add_matchstat'),
(26,'Can change match stat',7,'change_matchstat'),
(27,'Can delete match stat',7,'delete_matchstat'),
(28,'Can view match stat',7,'view_matchstat'),
(29,'Can add log entry',8,'add_logentry'),
(30,'Can change log entry',8,'change_logentry'),
(31,'Can delete log entry',8,'delete_logentry'),
(32,'Can view log entry',8,'view_logentry'),
(33,'Can add permission',9,'add_permission'),
(34,'Can change permission',9,'change_permission'),
(35,'Can delete permission',9,'delete_permission'),
(36,'Can view permission',9,'view_permission'),
(37,'Can add group',10,'add_group'),
(38,'Can change group',10,'change_group'),
(39,'Can delete group',10,'delete_group'),
(40,'Can view group',10,'view_group'),
(41,'Can add user',11,'add_user'),
(42,'Can change user',11,'change_user'),
(43,'Can delete user',11,'delete_user'),
(44,'Can view user',11,'view_user'),
(45,'Can add content type',12,'add_contenttype'),
(46,'Can change content type',12,'change_contenttype'),
(47,'Can delete content type',12,'delete_contenttype'),
(48,'Can view content type',12,'view_contenttype'),
(49,'Can add session',13,'add_session'),
(50,'Can change session',13,'change_session'),
(51,'Can delete session',13,'delete_session'),
(52,'Can view session',13,'view_session'),
(53,'Can add customer',14,'add_customer'),
(54,'Can change customer',14,'change_customer'),
(55,'Can delete customer',14,'delete_customer'),
(56,'Can view customer',14,'view_customer'),
(57,'Can add Token',17,'add_token'),
(58,'Can change Token',17,'change_token'),
(59,'Can delete Token',17,'delete_token'),
(60,'Can view Token',17,'view_token'),
(61,'Can add sys extra type',18,'add_sysextratype'),
(62,'Can change sys extra type',18,'change_sysextratype'),
(63,'Can delete sys extra type',18,'delete_sysextratype'),
(64,'Can view sys extra type',18,'view_sysextratype'),
(65,'Can add vw match stat',19,'add_vwmatchstat'),
(66,'Can change vw match stat',19,'change_vwmatchstat'),
(67,'Can delete vw match stat',19,'delete_vwmatchstat'),
(68,'Can view vw match stat',19,'view_vwmatchstat'),
(69,'Can add vw team point',20,'add_vwteampoint'),
(70,'Can change vw team point',20,'change_vwteampoint'),
(71,'Can delete vw team point',20,'delete_vwteampoint'),
(72,'Can view vw team point',20,'view_vwteampoint'),
(73,'Can add team match',21,'add_teammatch'),
(74,'Can change team match',21,'change_teammatch'),
(75,'Can delete team match',21,'delete_teammatch'),
(76,'Can view team match',21,'view_teammatch'),
(77,'Can add match stat extra',22,'add_matchstatextra'),
(78,'Can change match stat extra',22,'change_matchstatextra'),
(79,'Can delete match stat extra',22,'delete_matchstatextra'),
(80,'Can view match stat extra',22,'view_matchstatextra'),
(81,'Can add match stat',23,'add_matchstat'),
(82,'Can change match stat',23,'change_matchstat'),
(83,'Can delete match stat',23,'delete_matchstat'),
(84,'Can view match stat',23,'view_matchstat'),
(85,'Can add player',16,'add_player'),
(86,'Can change player',16,'change_player'),
(87,'Can delete player',16,'delete_player'),
(88,'Can view player',16,'view_player'),
(89,'Can add team player',24,'add_teamplayer'),
(90,'Can change team player',24,'change_teamplayer'),
(91,'Can delete team player',24,'delete_teamplayer'),
(92,'Can view team player',24,'view_teamplayer'),
(93,'Can add team',15,'add_team'),
(94,'Can change team',15,'change_team'),
(95,'Can delete team',15,'delete_team'),
(96,'Can view team',15,'view_team');

/*Data for the table `auth_user` */

insert  into `auth_user`(`id`,`password`,`last_login`,`is_superuser`,`username`,`first_name`,`last_name`,`email`,`is_staff`,`is_active`,`date_joined`) values 
(1,'pbkdf2_sha256$150000$HLO3Z76gQ5lW$0Tg5g2C2BQJ0b+yS7l5TKjeMyWB73a3Ec2H1yt3eSvk=','2019-11-24 04:43:30.659726',1,'admin','','','pankaj.gupta@knowarth.com',1,1,'2019-11-17 11:58:28.758488');

/*Data for the table `auth_user_groups` */

/*Data for the table `auth_user_user_permissions` */

/*Data for the table `authtoken_token` */

insert  into `authtoken_token`(`key`,`created`,`user_id`) values 
('8628ac2a89baa659c77f7044e5cd403ca5b9d74a','2019-11-20 05:55:34.725050',1);

/*Data for the table `django_admin_log` */

insert  into `django_admin_log`(`id`,`action_time`,`object_id`,`object_repr`,`action_flag`,`change_message`,`content_type_id`,`user_id`) values 
(1,'2019-11-17 12:00:09.874158','1','MI vs DD',1,'[{\"added\": {}}]',5,1),
(2,'2019-11-17 12:01:14.948479','2','MI vs Sunrisers',1,'[{\"added\": {}}]',5,1),
(3,'2019-11-17 12:02:20.531113','3','MI vs DD',1,'[{\"added\": {}}]',5,1),
(4,'2019-11-17 12:22:27.096242','1','MatchStat object (1)',1,'[{\"added\": {}}]',7,1),
(5,'2019-11-17 12:23:48.594083','2','MatchStat object (2)',1,'[{\"added\": {}}]',7,1),
(6,'2019-11-17 12:24:27.954440','3','MatchStat object (3)',1,'[{\"added\": {}}]',7,1),
(7,'2019-11-17 13:22:57.820613','4','MI vs Sunrisers',1,'[{\"added\": {}}]',5,1),
(8,'2019-11-18 08:09:21.307482','1','No Ball',1,'[{\"added\": {}}]',2,1),
(9,'2019-11-18 08:09:33.845282','2','Wide Ball',1,'[{\"added\": {}}]',2,1),
(10,'2019-11-18 08:09:46.897786','3','Leg Byes',1,'[{\"added\": {}}]',2,1),
(11,'2019-11-18 08:09:52.395632','4','Byes',1,'[{\"added\": {}}]',2,1),
(12,'2019-11-18 08:10:08.319483','5','Over Throw',1,'[{\"added\": {}}]',2,1),
(13,'2019-11-18 11:33:05.683506','4','MI vs Sunrisers - virat kohli',1,'[{\"added\": {}}]',7,1),
(14,'2019-11-20 07:06:15.087775','8','Rajasthan Royals',2,'[{\"changed\": {\"fields\": [\"team_name\"]}}]',15,1),
(15,'2019-11-20 07:06:30.560250','7','Chennai Super Kings',2,'[{\"changed\": {\"fields\": [\"team_name\"]}}]',15,1),
(16,'2019-11-20 07:06:43.166520','6','Kolkata Knight Riders',2,'[{\"changed\": {\"fields\": [\"team_name\"]}}]',15,1),
(17,'2019-11-20 07:06:50.764543','5','Kings XI Punjab',2,'[{\"changed\": {\"fields\": [\"team_name\"]}}]',15,1),
(18,'2019-11-20 07:07:00.227230','4','Royal Chellangers',2,'[{\"changed\": {\"fields\": [\"team_name\"]}}]',15,1),
(19,'2019-11-20 07:07:11.081978','4','Royal Challengers',2,'[{\"changed\": {\"fields\": [\"team_name\"]}}]',15,1),
(20,'2019-11-20 07:07:17.496938','3','Sunrisers Hyderabad',2,'[{\"changed\": {\"fields\": [\"team_name\"]}}]',15,1),
(21,'2019-11-20 07:07:26.316904','2','Delhi Capitals',2,'[{\"changed\": {\"fields\": [\"team_name\"]}}]',15,1),
(22,'2019-11-20 07:07:36.929969','1','Mumbai Indians',2,'[{\"changed\": {\"fields\": [\"team_name\"]}}]',15,1),
(23,'2019-11-20 07:09:29.842780','17','Mitchell Mcclenaghan',2,'[{\"changed\": {\"fields\": [\"first_name\", \"last_name\"]}}]',16,1),
(24,'2019-11-24 04:34:51.293808','8','Rajasthan Royals',2,'[{\"changed\": {\"fields\": [\"logo_url\"]}}]',15,1),
(25,'2019-11-24 04:37:24.310843','7','Chennai Super Kings',2,'[{\"changed\": {\"fields\": [\"logo_url\"]}}]',15,1),
(26,'2019-11-24 04:38:26.407749','6','Kolkata Knight Riders',2,'[{\"changed\": {\"fields\": [\"logo_url\"]}}]',15,1),
(27,'2019-11-24 04:39:14.084521','5','Kings XI Punjab',2,'[{\"changed\": {\"fields\": [\"logo_url\"]}}]',15,1),
(28,'2019-11-24 04:39:57.247120','4','Royal Challengers',2,'[{\"changed\": {\"fields\": [\"logo_url\"]}}]',15,1),
(29,'2019-11-24 04:40:34.172949','3','Sunrisers Hyderabad',2,'[{\"changed\": {\"fields\": [\"logo_url\"]}}]',15,1),
(30,'2019-11-24 04:40:45.841909','1','Mumbai Indians',2,'[]',15,1),
(31,'2019-11-24 04:41:18.093519','2','Delhi Capitals',2,'[{\"changed\": {\"fields\": [\"logo_url\"]}}]',15,1),
(32,'2019-11-24 06:10:06.022857','16','Yuvraj Singh',2,'[{\"changed\": {\"fields\": [\"first_name\", \"last_name\", \"image_uri\"]}}]',16,1),
(33,'2019-11-24 06:10:19.220850','2','Virat Kohli',2,'[{\"changed\": {\"fields\": [\"first_name\", \"last_name\", \"image_uri\"]}}]',16,1);

/*Data for the table `django_content_type` */

insert  into `django_content_type`(`id`,`app_label`,`model`) values 
(1,'cricketapi','player'),
(2,'cricketapi','sysextratype'),
(3,'cricketapi','team'),
(4,'cricketapi','teamplayer'),
(5,'cricketapi','teammatch'),
(6,'cricketapi','matchstatextra'),
(7,'cricketapi','matchstat'),
(8,'admin','logentry'),
(9,'auth','permission'),
(10,'auth','group'),
(11,'auth','user'),
(12,'contenttypes','contenttype'),
(13,'sessions','session'),
(14,'data','customer'),
(15,'team','team'),
(16,'player','player'),
(17,'authtoken','token'),
(18,'core','sysextratype'),
(19,'fixture','vwmatchstat'),
(20,'fixture','vwteampoint'),
(21,'fixture','teammatch'),
(22,'fixture','matchstatextra'),
(23,'fixture','matchstat'),
(24,'player','teamplayer');

/*Data for the table `django_migrations` */

insert  into `django_migrations`(`id`,`app`,`name`,`applied`) values 
(1,'contenttypes','0001_initial','2019-11-17 08:04:34.402727'),
(2,'auth','0001_initial','2019-11-17 08:04:34.783724'),
(3,'admin','0001_initial','2019-11-17 08:04:35.828755'),
(4,'admin','0002_logentry_remove_auto_add','2019-11-17 08:04:36.152734'),
(5,'admin','0003_logentry_add_action_flag_choices','2019-11-17 08:04:36.173745'),
(6,'contenttypes','0002_remove_content_type_name','2019-11-17 08:04:36.314158'),
(7,'auth','0002_alter_permission_name_max_length','2019-11-17 08:04:36.382426'),
(8,'auth','0003_alter_user_email_max_length','2019-11-17 08:04:36.452984'),
(9,'auth','0004_alter_user_username_opts','2019-11-17 08:04:36.483998'),
(10,'auth','0005_alter_user_last_login_null','2019-11-17 08:04:36.583644'),
(11,'auth','0006_require_contenttypes_0002','2019-11-17 08:04:36.590642'),
(12,'auth','0007_alter_validators_add_error_messages','2019-11-17 08:04:36.623596'),
(13,'auth','0008_alter_user_username_max_length','2019-11-17 08:04:36.706884'),
(14,'auth','0009_alter_user_last_name_max_length','2019-11-17 08:04:36.785744'),
(15,'auth','0010_alter_group_name_max_length','2019-11-17 08:04:36.872983'),
(16,'auth','0011_update_proxy_permissions','2019-11-17 08:04:36.907970'),
(17,'cricketapi','0001_initial','2019-11-17 08:04:37.546214'),
(18,'data','0001_initial','2019-11-17 08:04:38.707646'),
(19,'sessions','0001_initial','2019-11-17 08:04:38.792518'),
(20,'authtoken','0001_initial','2019-11-20 17:17:33.401498'),
(21,'authtoken','0002_auto_20160226_1747','2019-11-20 17:17:33.436477'),
(22,'core','0001_initial','2019-11-20 17:17:33.444472'),
(23,'team','0001_initial','2019-11-20 17:17:33.455466'),
(24,'player','0001_initial','2019-11-20 17:17:33.466461'),
(25,'fixture','0001_initial','2019-11-20 17:17:33.475455'),
(26,'fixture','0002_matchstat_team_player_id','2019-11-20 17:17:33.485450'),
(27,'fixture','0002_auto_20191121_1319','2019-11-21 07:49:28.490417'),
(28,'fixture','0003_auto_20191121_1325','2019-11-21 07:55:45.256090'),
(29,'core','0002_auto_20191121_1359','2019-11-21 08:29:52.517630'),
(30,'fixture','0004_auto_20191121_1359','2019-11-21 08:29:53.443590'),
(31,'team','0002_auto_20191121_1359','2019-11-21 08:29:53.754623'),
(32,'player','0002_auto_20191121_1359','2019-11-21 08:29:54.262970'),
(33,'fixture','0002_auto_20191124_1552','2019-11-24 10:22:29.308806');

/*Data for the table `django_session` */

insert  into `django_session`(`session_key`,`session_data`,`expire_date`) values 
('xzwaq3ec032jnih81ztxdaskmdf0emsg','MTM0ZjMxYTZmM2RkNzA4NmM2YzYzNGNiNThhNjQ1ZTVlZWU2YzYzNTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlYmU1MjJkMjkyNjZjOTg2ZjhlNDI5MTczZTQ0NWY1ODMwMTA4YzZiIn0=','2019-12-01 11:59:09.700988'),
('85v8wappw5pkbwsuoxvimtiqc6kwxclt','MTM0ZjMxYTZmM2RkNzA4NmM2YzYzNGNiNThhNjQ1ZTVlZWU2YzYzNTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlYmU1MjJkMjkyNjZjOTg2ZjhlNDI5MTczZTQ0NWY1ODMwMTA4YzZiIn0=','2019-12-02 05:08:55.473315'),
('hbb7rdd4qi3yh6d2dvtpmjr6i0yd8ndy','ZjdlMTYzODhhMTk2ZGU3MGE2YWU1YTBmNGRhY2ZhYjRiYzg3NWI4MTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyN2ZmYmQ2YTU5ZmQ2N2ExZmRmZmZkYjQ2ZjdlOGE3ZWQwOTI4MmY2In0=','2019-12-08 04:43:30.666722'),
('ez0vyadj2nujt8gjh6x8hujkar7xrbyw','ZjRhMDVlOWQ2ZGUxYTNlNzU5YjUxYjliOWRlYWZhMWFkZTUzZDhhZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4NWFlMzhjOTI3ZTY5MzI0ZGExYTJhZTllYWZlYzI1Y2EzMDM1ZTM4In0=','2019-12-04 19:05:50.307531'),
('kiciaxwqs4uet834g44scj4kl2f7bs1k','ZjRhMDVlOWQ2ZGUxYTNlNzU5YjUxYjliOWRlYWZhMWFkZTUzZDhhZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4NWFlMzhjOTI3ZTY5MzI0ZGExYTJhZTllYWZlYzI1Y2EzMDM1ZTM4In0=','2019-12-06 18:25:44.373414'),
('d3zb3vxf4kz690kcfocu9q2puorecazs','ZjdlMTYzODhhMTk2ZGU3MGE2YWU1YTBmNGRhY2ZhYjRiYzg3NWI4MTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyN2ZmYmQ2YTU5ZmQ2N2ExZmRmZmZkYjQ2ZjdlOGE3ZWQwOTI4MmY2In0=','2019-12-06 19:27:01.861075');

/*Data for the table `match_stat` */

insert  into `match_stat`(`id`,`batting_order`,`score_run`,`score_wicket`,`score_catch`,`score_runout`,`inning_start_datetime`,`inning_end_datetime`,`is_mom`,`is_any_violation`,`violation_id`,`team_match_id`,`team_player_id`) values 
(1,9,12,3,1,0,'2019-11-01 17:51:12.000000','2019-11-01 12:22:16.000000',0,0,0,1,0),
(2,1,155,0,3,1,'2019-11-01 17:52:27.000000','2019-11-01 12:23:37.000000',1,0,0,8,4),
(3,1,45,0,4,2,'2019-11-17 17:53:48.000000','2019-11-01 12:24:19.000000',0,0,0,17,3),
(4,1,72,1,2,1,'2019-11-18 17:02:15.000000','2019-11-18 11:32:54.000000',1,0,0,17,2),
(5,10,57,4,0,2,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,2,1),
(6,3,87,2,1,1,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,19,0),
(7,10,98,4,4,0,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,19,10000),
(8,4,73,1,1,4,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,19,4),
(9,3,88,2,4,1,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,2,5),
(10,7,61,4,2,1,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,6),
(11,4,34,2,2,1,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,5,7),
(12,10,71,3,2,1,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,8),
(13,0,34,2,4,2,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,5,9),
(14,10,26,2,3,3,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,10),
(15,5,55,1,1,1,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,17,11),
(16,2,28,3,0,4,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,12),
(17,10,53,3,4,1,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,17,13),
(18,10,64,2,1,2,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,19,14),
(19,5,0,2,1,1,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,2,15),
(20,5,64,3,2,0,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,17,16),
(21,4,50,1,1,3,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,17,17),
(22,8,40,3,3,3,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,5,18),
(23,10,41,1,0,2,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,19),
(24,8,93,2,2,3,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,20),
(25,4,4,4,0,0,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,21),
(26,10,60,0,4,1,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,22),
(27,9,16,1,0,1,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,5,23),
(28,0,39,4,3,0,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,19,24),
(29,10,32,3,3,1,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,17,25),
(30,10,33,3,3,4,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,19,26),
(31,7,54,3,1,3,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,2,27),
(32,0,84,1,2,4,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,19,28),
(33,3,52,3,0,3,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,5,29),
(34,5,79,2,3,0,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,30),
(35,2,11,4,4,2,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,5,31),
(36,3,84,1,1,4,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,32),
(37,3,59,0,1,4,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,33),
(38,10,13,4,1,1,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,34),
(39,1,65,4,0,2,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,35),
(40,8,44,4,1,2,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,36),
(41,1,92,1,1,3,'2019-11-24 14:02:19.000000','2019-11-24 14:02:19.000000',0,0,0,1,37),
(42,10,67,2,4,2,'2019-11-24 14:35:23.000000','2019-11-24 14:35:23.000000',0,0,0,2,6),
(43,5,50,4,0,2,'2019-11-24 14:35:23.000000','2019-11-24 14:35:23.000000',0,0,0,2,8),
(44,1,46,4,3,0,'2019-11-24 14:35:23.000000','2019-11-24 14:35:23.000000',0,0,0,2,12),
(45,3,46,1,0,1,'2019-11-24 14:35:23.000000','2019-11-24 14:35:23.000000',0,0,0,2,19),
(46,1,11,0,0,4,'2019-11-24 14:35:23.000000','2019-11-24 14:35:23.000000',0,0,0,2,21),
(47,8,89,0,1,2,'2019-11-24 14:35:23.000000','2019-11-24 14:35:23.000000',0,0,0,2,30),
(48,2,40,1,2,1,'2019-11-24 14:35:23.000000','2019-11-24 14:35:23.000000',0,0,0,2,33),
(49,5,22,3,4,4,'2019-11-24 14:35:23.000000','2019-11-24 14:35:23.000000',0,0,0,2,34),
(50,3,74,3,3,0,'2019-11-24 14:35:23.000000','2019-11-24 14:35:23.000000',0,0,0,2,35),
(51,3,37,4,2,3,'2019-11-24 14:35:23.000000','2019-11-24 14:35:23.000000',0,0,0,2,36),
(52,3,46,1,4,1,'2019-11-24 14:37:45.000000','2019-11-24 14:37:45.000000',0,0,0,3,10),
(53,4,79,4,4,3,'2019-11-24 14:37:45.000000','2019-11-24 14:37:45.000000',0,0,0,3,20),
(54,8,88,0,4,0,'2019-11-24 14:37:45.000000','2019-11-24 14:37:45.000000',0,0,0,3,22),
(55,5,36,1,3,2,'2019-11-24 14:37:45.000000','2019-11-24 14:37:45.000000',0,0,0,3,32),
(56,2,68,3,4,4,'2019-11-24 14:37:45.000000','2019-11-24 14:37:45.000000',0,0,0,3,37),
(57,2,27,3,2,3,'2019-11-24 14:38:13.000000','2019-11-24 14:38:13.000000',0,0,0,3,6),
(58,7,31,3,0,0,'2019-11-24 14:38:13.000000','2019-11-24 14:38:13.000000',0,0,0,3,8),
(59,8,52,1,2,4,'2019-11-24 14:38:13.000000','2019-11-24 14:38:13.000000',0,0,0,3,12),
(60,2,55,0,2,1,'2019-11-24 14:38:13.000000','2019-11-24 14:38:13.000000',0,0,0,3,19),
(61,3,33,4,0,2,'2019-11-24 14:38:13.000000','2019-11-24 14:38:13.000000',0,0,0,3,21),
(62,6,56,0,4,1,'2019-11-24 14:38:13.000000','2019-11-24 14:38:13.000000',0,0,0,3,30),
(63,5,69,4,4,2,'2019-11-24 14:38:13.000000','2019-11-24 14:38:13.000000',0,0,0,3,33),
(64,9,67,4,1,0,'2019-11-24 14:38:13.000000','2019-11-24 14:38:13.000000',0,0,0,3,34),
(65,6,39,1,2,1,'2019-11-24 14:38:13.000000','2019-11-24 14:38:13.000000',0,0,0,3,35),
(66,7,87,1,4,3,'2019-11-24 14:38:13.000000','2019-11-24 14:38:13.000000',0,0,0,3,36);

/*Data for the table `match_stat_extra` */

insert  into `match_stat_extra`(`id`,`score_extra`,`sys_extra_type_id`,`team_match_id`,`team_id`) values 
(1,5,1,1,1),
(2,12,2,1,2),
(3,3,3,1,1),
(4,4,4,1,2),
(5,6,5,1,2);

/*Data for the table `player` */

insert  into `player`(`id`,`first_name`,`last_name`,`image_uri`,`jersey_number`,`country_id`) values 
(1,'Md','Shami','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',5,9),
(2,'Virat','Kohli','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',42,3),
(3,'Jusprit','Bumrah','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',94,2),
(4,'Rohit','Shrma','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',43,8),
(5,'Widdhi','Saha','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',37,4),
(6,'R','Aswin','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',53,3),
(7,'A','Rahane','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',57,5),
(8,'K','Pollard','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',24,7),
(9,'Hardik','Pandya','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',52,2),
(10,'Lasith','Malinga','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',88,1),
(11,'Evin','Lewis','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',86,5),
(12,'Ishan','Kishan','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',66,1),
(13,'Mayank','Markande','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',73,1),
(14,'Krunal','Pandya','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',65,2),
(15,'Suryakumar','Yadav','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',6,7),
(16,'Yuvraj','Singh','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',38,2),
(17,'Mitchell','Mcclenaghan','https://maveric-systems.com/wp-content/uploads/2019/08/dummy-image-testimonial-300x298.jpg',69,9),
(18,'Ambati','Rayadu','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/4176.png',72,99),
(19,'Faf','Du Plesis','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/28891.png',101,99),
(20,'Kedar','Jadhav','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/4532.png',36,99),
(21,'Murali','Kartik','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/4531.png',28,99),
(22,'Ruturaj','Pal','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/66584.png',33,99),
(23,'Suresh','Raina','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/3700.png',81,99),
(24,'Dwayne','Brabo','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/3556.png',7,99),
(25,'Mitchell','Marshal','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/57903.png',93,99),
(26,'Ravindra','Jadeja','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/3850.png',50,99),
(27,'Shane','Watson','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/3355.png',64,99),
(28,'MS','Dhoni','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/3676.png',82,99),
(29,'Narayan','Singh','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/66451.png',13,99),
(30,'Deepak','Chahar','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/59547.png',18,99),
(31,'Harbhajan','Singh','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/1367.png',54,99),
(32,'Imran','Tahir','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/4945.png',11,99),
(33,'Karn','Thapar','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/5090.png',96,99),
(34,'KM','Sharma','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/67709.png',51,99),
(35,'Lungi','D','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/64225.png',59,99),
(36,'Monu','Sharma','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/64510.png',49,99),
(37,'Shardul','Kulkarni','https://xmlns.cricketnext.com/cktnxt/scorecard/crk_player_images/players/128X128/63345.png',67,99);

/*Data for the table `sys_extra_type` */

insert  into `sys_extra_type`(`id`,`entra_type_name`,`i_active`) values 
(1,'No Ball',1),
(2,'Wide Ball',1),
(3,'Leg Byes',1),
(4,'Byes',1),
(5,'Over Throw',1);

/*Data for the table `team` */

insert  into `team`(`id`,`team_name`,`logo_url`,`club_state`) values 
(1,'Mumbai Indians','http://cricket.sporting99.com/ipl/pics/mumbai-indians.jpg','Mumbai'),
(2,'Delhi Capitals','https://upload.wikimedia.org/wikipedia/en/thumb/f/f5/Delhi_Capitals_Logo.svg/1200px-Delhi_Capitals_Logo.svg.png','Delhi'),
(3,'Sunrisers Hyderabad','https://upload.wikimedia.org/wikipedia/en/thumb/8/81/Sunrisers_Hyderabad.svg/1200px-Sunrisers_Hyderabad.svg.png','Hyderabad'),
(4,'Royal Challengers','https://upload.wikimedia.org/wikipedia/en/thumb/9/9a/Royal_Challengers_Bangalore_Logo_2016.svg/1200px-Royal_Challengers_Bangalore_Logo_2016.svg.png','Banglore'),
(5,'Kings XI Punjab','https://upload.wikimedia.org/wikipedia/en/thumb/e/e7/Kings_XI_Punjab_logo.svg/200px-Kings_XI_Punjab_logo.svg.png','Punjab'),
(6,'Kolkata Knight Riders','https://upload.wikimedia.org/wikipedia/en/thumb/4/4c/Kolkata_Knight_Riders_Logo.svg/200px-Kolkata_Knight_Riders_Logo.svg.png','Kolkata'),
(7,'Chennai Super Kings','https://www.kreedon.com/wp-content/uploads/2018/12/1-8552f811e95b998d9505c43a9828c6d6-696x392.jpg','Chennai'),
(8,'Rajasthan Royals','https://upload.wikimedia.org/wikipedia/hi/thumb/6/60/Rajasthan_Royals_Logo.svg/300px-Rajasthan_Royals_Logo.svg.png','Rajasthan');

/*Data for the table `team_match` */

insert  into `team_match`(`id`,`match_type`,`venue`,`match_start_date`,`match_end_date`,`a_team_point`,`b_team_point`,`toss_winner_choice`,`match_result`,`win_margin`,`a_team_id`,`b_team_id`,`toss_winner_team_id`,`winner_team_id`) values 
(1,'T2','Mumbai','2019-11-01','2019-11-01',2,0,'BAT','DC','10',1,2,1,1),
(2,'T2','Hyderabad','2019-11-02','2019-11-02',2,0,'BAT','DC','15',1,3,1,1),
(3,'T2','Delhi','2019-11-03','2019-11-03',2,0,'BOW','DC','4',2,1,2,2),
(4,'T2','Mumbai','2019-11-04','2019-11-04',1,1,'BAT','TI','0',1,3,1,3),
(5,'T2','Mumbai','2019-11-01','2019-11-01',2,0,'BAT','DC','7',4,3,4,4),
(6,'T2','Mumbai','2019-11-01','2019-11-01',0,2,'BAT','DC','9',3,4,4,4),
(7,'T2','Mumbai','2019-11-01','2019-11-01',2,0,'BAT','DC','8',5,6,6,5),
(8,'T2','Mumbai','2019-11-01','2019-11-01',0,2,'BAT','DC','6',5,7,6,7),
(9,'T2','Mumbai','2019-11-01','2019-11-01',0,2,'BAT','DC','4',6,1,6,1),
(10,'T2','Mumbai','2019-11-01','2019-11-01',2,0,'BAT','DC','10',1,4,4,1),
(11,'T2','Mumbai','2019-11-01','2019-11-01',2,0,'BAT','DC','10',3,5,4,3),
(12,'T2','Mumbai','2019-11-01','2019-11-01',2,0,'BAT','DC','10',7,4,7,7),
(13,'T2','Mumbai','2019-11-01','2019-11-01',2,0,'BAT','DC','10',1,4,4,1),
(14,'T2','Mumbai','2019-11-01','2019-11-01',2,0,'BAT','DC','5',2,5,5,2),
(15,'T2','Mumbai','2019-11-01','2019-11-01',0,2,'BAT','DC','9',5,8,8,8),
(16,'T2','Mumbai','2019-11-01','2019-11-01',2,0,'BAT','DC','10',4,2,2,4),
(17,'T2','Mumbai','2019-11-01','2019-11-01',0,2,'BAT','DC','3',6,8,6,8),
(18,'T2','Mumbai','2019-11-01','2019-11-01',2,0,'BAT','DC','3',6,3,3,6),
(19,'T2','Mumbai','2019-11-01','2019-11-01',2,0,'BAT','DC','10',5,7,7,5);

/*Data for the table `team_player` */

insert  into `team_player`(`id`,`contract_start_date`,`contract_end_date`,`is_active`,`player_id`,`team_id`) values 
(1,'2019-01-01','2019-12-31',1,1,3),
(2,'2019-01-01','2019-12-31',1,2,6),
(3,'2019-01-01','2019-12-31',1,3,6),
(4,'2019-01-01','2019-12-31',1,4,7),
(5,'2019-01-01','2019-12-31',1,5,3),
(6,'2019-01-01','2019-12-31',1,6,1),
(7,'2019-01-01','2019-12-31',1,7,4),
(8,'2019-01-01','2019-12-31',1,8,1),
(9,'2019-01-01','2019-12-31',1,9,4),
(10,'2019-01-01','2019-12-31',1,10,2),
(11,'2019-01-01','2019-12-31',1,11,6),
(12,'2019-01-01','2019-12-31',1,12,1),
(13,'2019-01-01','2019-12-31',1,13,6),
(14,'2019-01-01','2019-12-31',1,14,5),
(15,'2019-01-01','2019-12-31',1,15,3),
(16,'2019-01-01','2019-12-31',1,16,6),
(17,'2019-01-01','2019-12-31',1,17,6),
(18,'2019-01-01','2019-12-31',1,18,4),
(19,'2019-01-01','2019-12-31',1,19,1),
(20,'2019-01-01','2019-12-31',1,20,2),
(21,'2019-01-01','2019-12-31',1,21,1),
(22,'2019-01-01','2019-12-31',1,22,2),
(23,'2019-01-01','2019-12-31',1,23,4),
(24,'2019-01-01','2019-12-31',1,24,5),
(25,'2019-01-01','2019-12-31',1,25,6),
(26,'2019-01-01','2019-12-31',1,26,5),
(27,'2019-01-01','2019-12-31',1,27,3),
(28,'2019-01-01','2019-12-31',1,28,7),
(29,'2019-01-01','2019-12-31',1,29,4),
(30,'2019-01-01','2019-12-31',1,30,1),
(31,'2019-01-01','2019-12-31',1,31,4),
(32,'2019-01-01','2019-12-31',1,32,2),
(33,'2019-01-01','2019-12-31',1,33,1),
(34,'2019-01-01','2019-12-31',1,34,1),
(35,'2019-01-01','2019-12-31',1,35,1),
(36,'2019-01-01','2019-12-31',1,36,1),
(37,'2019-01-01','2019-12-31',1,37,2);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
