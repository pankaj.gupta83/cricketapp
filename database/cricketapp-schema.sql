/*
SQLyog Community
MySQL - 5.7.26 : Database - cricketapp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `auth_group` */

DROP TABLE IF EXISTS `auth_group`;

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `auth_group_permissions` */

DROP TABLE IF EXISTS `auth_group_permissions`;

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_group_id_b120cbf9` (`group_id`),
  KEY `auth_group_permissions_permission_id_84c5c92e` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `auth_permission` */

DROP TABLE IF EXISTS `auth_permission`;

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  KEY `auth_permission_content_type_id_2f476e4b` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;

/*Table structure for table `auth_user` */

DROP TABLE IF EXISTS `auth_user`;

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `auth_user_groups` */

DROP TABLE IF EXISTS `auth_user_groups`;

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_user_id_6a12ed8b` (`user_id`),
  KEY `auth_user_groups_group_id_97559544` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `auth_user_user_permissions` */

DROP TABLE IF EXISTS `auth_user_user_permissions`;

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_user_id_a95ead1b` (`user_id`),
  KEY `auth_user_user_permissions_permission_id_1fbb5f2c` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `authtoken_token` */

DROP TABLE IF EXISTS `authtoken_token`;

CREATE TABLE `authtoken_token` (
  `key` varchar(40) NOT NULL,
  `created` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`key`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `django_admin_log` */

DROP TABLE IF EXISTS `django_admin_log`;

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Table structure for table `django_content_type` */

DROP TABLE IF EXISTS `django_content_type`;

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Table structure for table `django_migrations` */

DROP TABLE IF EXISTS `django_migrations`;

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Table structure for table `django_session` */

DROP TABLE IF EXISTS `django_session`;

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `match_stat` */

DROP TABLE IF EXISTS `match_stat`;

CREATE TABLE `match_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batting_order` int(11) NOT NULL,
  `score_run` int(11) NOT NULL,
  `score_wicket` int(11) NOT NULL,
  `score_catch` int(11) NOT NULL,
  `score_runout` int(11) NOT NULL,
  `inning_start_datetime` datetime(6) NOT NULL,
  `inning_end_datetime` datetime(6) NOT NULL,
  `is_mom` tinyint(1) NOT NULL,
  `is_any_violation` int(11) NOT NULL,
  `violation_id` int(11) DEFAULT NULL,
  `team_match_id` int(11) DEFAULT NULL,
  `team_player_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `match_stat_team_match_id_team_player_id_432e7a04_uniq` (`team_match_id`,`team_player_id`),
  KEY `match_stat_team_match_id_19243a21` (`team_match_id`),
  KEY `match_stat_team_player_id_60c04a57` (`team_player_id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

/*Table structure for table `match_stat_extra` */

DROP TABLE IF EXISTS `match_stat_extra`;

CREATE TABLE `match_stat_extra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `score_extra` int(11) NOT NULL,
  `sys_extra_type_id` int(11) DEFAULT NULL,
  `team_match_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `match_stat_extra_sys_extra_type_id_team_m_21acd361_uniq` (`sys_extra_type_id`,`team_match_id`,`team_id`),
  KEY `match_stat_extra_sys_extra_type_id_562b83d2` (`sys_extra_type_id`),
  KEY `match_stat_extra_team_match_id_f8a6e598` (`team_match_id`),
  KEY `match_stat_extra_team_id_ff0f9e6f` (`team_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `player` */

DROP TABLE IF EXISTS `player`;

CREATE TABLE `player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `image_uri` varchar(999) NOT NULL,
  `jersey_number` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `player_first_n_d46659_idx` (`first_name`),
  KEY `player_last_na_771626_idx` (`last_name`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

/*Table structure for table `sys_extra_type` */

DROP TABLE IF EXISTS `sys_extra_type`;

CREATE TABLE `sys_extra_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entra_type_name` varchar(255) NOT NULL,
  `i_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entra_type_name` (`entra_type_name`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `team` */

DROP TABLE IF EXISTS `team`;

CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(60) NOT NULL,
  `logo_url` varchar(999) NOT NULL,
  `club_state` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_name` (`team_name`),
  KEY `team_club_st_4c7808_idx` (`club_state`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `team_match` */

DROP TABLE IF EXISTS `team_match`;

CREATE TABLE `team_match` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `match_type` varchar(2) NOT NULL,
  `venue` varchar(999) NOT NULL,
  `match_start_date` date NOT NULL,
  `match_end_date` date NOT NULL,
  `a_team_point` int(11) NOT NULL,
  `b_team_point` int(11) NOT NULL,
  `toss_winner_choice` varchar(3) NOT NULL,
  `match_result` varchar(2) NOT NULL,
  `win_margin` varchar(999) NOT NULL,
  `a_team_id` int(11) DEFAULT NULL,
  `b_team_id` int(11) DEFAULT NULL,
  `toss_winner_team_id` int(11) DEFAULT NULL,
  `winner_team_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `team_match_match_t_ae38f9_idx` (`match_type`),
  KEY `team_match_a_team_id_8224545e` (`a_team_id`),
  KEY `team_match_b_team_id_315e6a25` (`b_team_id`),
  KEY `team_match_toss_winner_team_id_0b02a6e2` (`toss_winner_team_id`),
  KEY `team_match_winner_team_id_12b59b9b` (`winner_team_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Table structure for table `team_player` */

DROP TABLE IF EXISTS `team_player`;

CREATE TABLE `team_player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_start_date` date NOT NULL,
  `contract_end_date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `player_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_player_team_id_player_id_144dc653_uniq` (`team_id`,`player_id`),
  KEY `team_player_player_id_6e848eb3` (`player_id`),
  KEY `team_player_team_id_031b9c1d` (`team_id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

/* Function  structure for function  `get_earned_points` */

/*!50003 DROP FUNCTION IF EXISTS `get_earned_points` */;
DELIMITER $$

/*!50003 CREATE FUNCTION `get_earned_points`(p_team_match_id INT(11), p_team_id INT(11)) RETURNS int(11)
    DETERMINISTIC
BEGIN 
	DECLARE v_points INTEGER(4);
	SELECT (IF(a_team_id = p_team_id, a_team_point, 0) + IF(b_team_id = p_team_id, b_team_point, 0)) INTO v_points 
	FROM team_match 
	WHERE ((a_team_id = p_team_id OR b_team_id = p_team_id)) AND id = p_team_match_id;
	
	RETURN IFNULL(v_points, 0);
END */$$
DELIMITER ;

/*Table structure for table `vw_match_stat` */

DROP TABLE IF EXISTS `vw_match_stat`;

/*!50001 DROP VIEW IF EXISTS `vw_match_stat` */;
/*!50001 DROP TABLE IF EXISTS `vw_match_stat` */;

/*!50001 CREATE TABLE  `vw_match_stat`(
 `player_id` int(11) ,
 `first_name` varchar(60) ,
 `last_name` varchar(60) ,
 `image_uri` varchar(999) ,
 `jersey_number` int(11) ,
 `country_id` int(11) ,
 `team_id` int(11) ,
 `team_name` varchar(60) ,
 `logo_url` varchar(999) ,
 `club_state` varchar(100) ,
 `id` int(11) ,
 `batting_order` int(11) ,
 `score_run` int(11) ,
 `score_wicket` int(11) ,
 `score_catch` int(11) ,
 `score_runout` int(11) ,
 `inning_start_datetime` datetime(6) ,
 `inning_end_datetime` datetime(6) ,
 `is_mom` tinyint(1) ,
 `is_any_violation` int(11) ,
 `violation_id` int(11) ,
 `team_player_id` int(11) ,
 `team_match_id` int(11) 
)*/;

/*Table structure for table `vw_team_point` */

DROP TABLE IF EXISTS `vw_team_point`;

/*!50001 DROP VIEW IF EXISTS `vw_team_point` */;
/*!50001 DROP TABLE IF EXISTS `vw_team_point` */;

/*!50001 CREATE TABLE  `vw_team_point`(
 `id` int(11) ,
 `team_name` varchar(60) ,
 `logo_url` varchar(999) ,
 `club_state` varchar(100) ,
 `total_match` bigint(21) ,
 `win_match` decimal(23,0) ,
 `lost_match` decimal(23,0) ,
 `tied_match` decimal(23,0) ,
 `earned_points` decimal(32,0) 
)*/;

/*View structure for view vw_match_stat */

/*!50001 DROP TABLE IF EXISTS `vw_match_stat` */;
/*!50001 DROP VIEW IF EXISTS `vw_match_stat` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_match_stat` AS select `player`.`id` AS `player_id`,`player`.`first_name` AS `first_name`,`player`.`last_name` AS `last_name`,`player`.`image_uri` AS `image_uri`,`player`.`jersey_number` AS `jersey_number`,`player`.`country_id` AS `country_id`,`team`.`id` AS `team_id`,`team`.`team_name` AS `team_name`,`team`.`logo_url` AS `logo_url`,`team`.`club_state` AS `club_state`,`match_stat`.`id` AS `id`,`match_stat`.`batting_order` AS `batting_order`,`match_stat`.`score_run` AS `score_run`,`match_stat`.`score_wicket` AS `score_wicket`,`match_stat`.`score_catch` AS `score_catch`,`match_stat`.`score_runout` AS `score_runout`,`match_stat`.`inning_start_datetime` AS `inning_start_datetime`,`match_stat`.`inning_end_datetime` AS `inning_end_datetime`,`match_stat`.`is_mom` AS `is_mom`,`match_stat`.`is_any_violation` AS `is_any_violation`,`match_stat`.`violation_id` AS `violation_id`,`match_stat`.`team_player_id` AS `team_player_id`,`match_stat`.`team_match_id` AS `team_match_id` from ((((`match_stat` left join `team_player` on((`match_stat`.`team_player_id` = `team_player`.`id`))) left join `team_match` on((`match_stat`.`team_match_id` = `team_match`.`id`))) left join `team` on((`team_player`.`team_id` = `team`.`id`))) left join `player` on((`team_player`.`player_id` = `player`.`id`))) */;

/*View structure for view vw_team_point */

/*!50001 DROP TABLE IF EXISTS `vw_team_point` */;
/*!50001 DROP VIEW IF EXISTS `vw_team_point` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_team_point` AS select `team`.`id` AS `id`,`team`.`team_name` AS `team_name`,`team`.`logo_url` AS `logo_url`,`team`.`club_state` AS `club_state`,count(`team_match`.`id`) AS `total_match`,sum(if(((`team_match`.`winner_team_id` = `team`.`id`) and (`team_match`.`match_result` = 'DC')),1,0)) AS `win_match`,sum(if(((`team_match`.`winner_team_id` <> `team`.`id`) and (`team_match`.`match_result` = 'DC')),1,0)) AS `lost_match`,sum(if((`team_match`.`match_result` = 'TI'),1,0)) AS `tied_match`,sum(`get_earned_points`(`team_match`.`id`,`team`.`id`)) AS `earned_points` from (`team` left join `team_match` on(((`team`.`id` = `team_match`.`a_team_id`) or (`team`.`id` = `team_match`.`b_team_id`)))) group by `team`.`id` */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
