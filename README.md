# Cricketapp
Cricket scoring app by using python Django REST API & Angular 7 


#Software Environment
---------------------------------------------------------------------------
1. Node - 10.6.0
2. Angular - 7.3.*
3. Mysql - 5.7
4. Python - 3.8
5. pip 19.3.1 - Use below link to install pip
    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    python get-pip.py
    pip -V
6. Django - 2.2.7

#Steps to setup application
-------------------------------------------------------------------------------
Download Code-: https://gitlab.com/pankaj.gupta83/cricketapp
OR
Clone repo : git clone https://gitlab.com/pankaj.gupta83/cricketapp.git cricketapp


#Part A - Setup Backend Python application 
--------------------------------------------------------------------------------
1. Create Database  with following credentials -
User Name : root
Password  : <none> 
DB Name   : cricketapp 
Or change DB  credentials into backend\cricketapi\cricketapi\settings.py file. 

2. DB Schema        : Import database\cricketapp-schema.sql into above DB. 
3. DB Sample Data   : Import database\cricketapp-data.sql into above DB. 

Now cd backend\cricketapi
run : python manage.py runserver
Open 127.0.0.1:8000 or http://localhost:8000/admin into browser for admin Panel access or any data operations


#Part B - Setup Front end app - Angular Application
-------------------------------------------------------------------------------------
cd frontend
npm install
ng serve
Open http://localhost:4200/ into chrome browser.
Click on login, It will use auth API to authonticate, without login app will not work.
For demo i have store credentials into environment.ts file, If required, you can change same.

#Login Credentials 
---------------------------------------------------------------------------------------
URL [Backend/Admin]: http://127.0.0.1:8000/admin/
User Name : admin
password : cric@2019
App URL [Frontend]: http://127.0.0.1:4200/

#Unit Test Cases
---------------------------------------------------------------------------------------
1. Now Create database "cricketapp_test" with same user.
2. Import database\cricketapp_test-schema.sql into above DB.
3. cd backend\cricketapi
4. python manage.py test --keepdb 
5. Output will be look like below text : 

    Ran 8 tests in 1.652s

    OK
    Preserving test database for alias 'default'...

# Application Structure 
----------------------------------------------------------
1. Backend - Python/Django


2. Frontend - Angular