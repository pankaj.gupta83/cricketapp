import { Component, OnInit } from '@angular/core';
import { FixturesService } from '../fixtures.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


@Component({
  selector: 'app-fixture-list',
  templateUrl: './fixture-list.component.html',
  styleUrls: ['./fixture-list.component.css']
})
export class FixtureListComponent implements OnInit {

  constructor(private router:ActivatedRoute, private approuter:Router, private fixtureservice: FixturesService) { }

  fixtureLists: any
  teamId;
  ngOnInit() {
    this.router.params.subscribe(
      (params)=>{
        this.teamId = params.teamId;
        this.getFixturesList();
      }
    );
    //this.getFixturesList("", {})
  }

  getFixturesList() {
    this.fixtureservice.getFixtureList(this.teamId, {})
      .subscribe(data => {
        this.fixtureLists = data;
      }, err => {
        console.log(err)
      })
  }

  getFixtureDetail(fixureId){
    let entityType = 'match'
    this.approuter.navigate(["fixtures/detail", fixureId, entityType]);
  }
}
