import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FixtureListComponent } from './fixture-list/fixture-list.component';
import { FixtureDetailComponent } from './fixture-detail/fixture-detail.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FixtureListComponent, FixtureDetailComponent]
})
export class FixturesModule { }
