import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FixturesService } from '../fixtures.service';

@Component({
  selector: 'app-fixture-detail',
  templateUrl: './fixture-detail.component.html',
  styleUrls: ['./fixture-detail.component.css']
})
export class FixtureDetailComponent implements OnInit {

  constructor(private routeract:ActivatedRoute, private router:Router, private fixturesservice: FixturesService) { }

  fixtureId: any
  entityType: any
  fixtureDetails: any
  fixtureExtras: any
  rowsCount:number
  rowsCountExtras: number
  
  ngOnInit() {
    this.routeract.params.subscribe(
      (params)=>{
        this.fixtureId = params.fixtureId;
        this.entityType = params.entityType;
        
        this.getFixtureDetail();

        if(this.entityType == 'match')
          this.getFixtureExtras();
      }
    );
  }

  getFixtureDetail() {
    this.fixturesservice.getFixtureDetail(this.fixtureId, this.entityType)
      .subscribe(data => {
        this.fixtureDetails = data;
        this.rowsCount = this.fixtureDetails.length;
        console.log(this.rowsCount)
      }, err => {
        console.log(err)
      })
  }

  getFixtureExtras() {
    this.fixturesservice.getFixtureExtras(this.fixtureId)
      .subscribe(data => {
        this.fixtureExtras = data;
        this.rowsCountExtras = data.length;
      }, err => {
        console.log(err)
      })
  }
  getPlayerDetail(playerId){
    this.router.navigate(["fixtures/detail", playerId, 'player']);
  }

  getTeamDetail(teamId){
    this.router.navigate(["teams/detail", teamId]);
  }


}
