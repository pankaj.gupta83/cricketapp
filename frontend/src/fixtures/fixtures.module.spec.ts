import { FixturesModule } from './fixtures.module';

describe('FixturesModule', () => {
  let fixturesModule: FixturesModule;

  beforeEach(() => {
    fixturesModule = new FixturesModule();
  });

  it('should create an instance', () => {
    expect(fixturesModule).toBeTruthy();
  });
});
