import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FixturesService {

  constructor(private apiservice:ApiService) { }

  getFixtureList(teamId, requestParams: any): Observable<any>{

    let requestUrl = '/teammatches/';

    if(teamId !== undefined && teamId != '')
      requestUrl = requestUrl + '?team_id='+teamId

    return this.apiservice.get(requestUrl, requestParams);
  }

  getFixtureDetail(fixtureId: any, entityType: any): Observable<any>{
    
    let requestUrl = '/matchdetails/'
    if(entityType == 'player')
      requestUrl = requestUrl + '?player_id='+fixtureId
    else if(entityType == 'match')   
      requestUrl = requestUrl + '?match_id='+fixtureId
    
    return this.apiservice.get(requestUrl, fixtureId);
  }

  getFixtureExtras(fixtureId: any): Observable<any>{
    
    let requestUrl = '/matchstatextras/?'

    if(fixtureId != '')   
      requestUrl = requestUrl + 'match_id='+fixtureId
    
    return this.apiservice.get(requestUrl, fixtureId);
  }


}
