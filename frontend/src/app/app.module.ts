import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRouterModule } from './/app-router.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TeamsModule } from 'src/teams/teams.module';
import { PlayersModule } from 'src/players/players.module';
import { FixturesModule } from 'src/fixtures/fixtures.module';
import { ApiInterceptor } from './shared/api.interceptor.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRouterModule,
    TeamsModule,
    PlayersModule,
    FixturesModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass:ApiInterceptor, multi:true}
  ],
  bootstrap: [AppComponent]

})
export class AppModule { }