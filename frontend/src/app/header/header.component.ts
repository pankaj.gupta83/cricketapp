import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { environment } from 'src/environments/environment';
import { ApiStorageService } from '../shared/services/storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  isLogin = false
  constructor(private authservice:AuthService, private storageservice:ApiStorageService) { }

  
  ngOnInit() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser && currentUser.token){
      this.isLogin = true
    }
  }

  userLogin(){
    let credentials = {username: environment.api_user, password: environment.api_pwd}
    this.authservice.login(credentials).subscribe(data=>{
      if(data.token != ""){
        let userToken = {'token':data.token,'accessToken':data.token} 
        /* 
        this.storageservice.setItem('currentUser', JSON.stringify(userToken)).subscribe(data=>{
          console.log('Token stored successfully....')
        })      
        */
        this.storageservice.setLocalStorage('currentUser', JSON.stringify(userToken));

        this.isLogin = true;
      }
    }, err=>{
      console.log('Login Error...', err)
    })
  }

  userLogout(){
    if(this.authservice.isAuthenticated() === true)
    {
      this.authservice.logout()
      this.isLogin = false;
    }
  }
}
