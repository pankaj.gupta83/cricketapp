import { Injectable } from '@angular/core';
import { Observable,  Subject, BehaviorSubject, from } from 'rxjs';
import * as localforage from 'localforage';

@Injectable({
  providedIn: 'root'
})
export class ApiStorageService {
 
  constructor() {}
  
  /**
   *
   * @param key
   * @param value
   * @returns {any}
   */
  public setItem<T>(key: string, value: T): Observable<T> {
    return from(localforage.setItem(key, value));
  }

  /**
   *
   * @param key
   * @returns {any}
   */
  public getItem<T>(key: string): Observable<T> {
    return from(localforage.getItem(key));
  }

  /**
   *
   * @param key
   * @returns {any}
   */
  public removeItem(key: string): Observable<void> {
    return from(localforage.removeItem(key));
  }

  public getLocalStorage(key: string): string {
    return localStorage.getItem(key);
  }

  public setLocalStorage(key: string, value: string): void {
    return localStorage.setItem(key, value);
  }

  public clearLocalStorage() {
    return localStorage.clear();
  }
}
