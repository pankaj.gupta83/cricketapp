import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private message: string;

  constructor(private _router: Router, private apiservice:ApiService) { }

  /**
   * this is used to clear anything that needs to be removed
   */
  clear(): void {
    localStorage.clear();
  }

  /**
   * check for expiration and if token is still existing or not
   * @return {boolean}
   */
  isAuthenticated(): boolean {

    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    
    if(currentUser != null)
      return currentUser.accessToken != null && !this.isTokenExpired();
    else 
      return true  
  }

  // simulate jwt token is valid
  isTokenExpired(): boolean {
    return false;
  }

  /**
   * this is used to clear local storage and also the route to login
   */
  logout(): void {
    this.clear();
    this._router.navigate(['/']);
  }

  login(credentails): Observable <any>{

    return this.apiservice.post('/api-token-auth/', credentails);
  }

}