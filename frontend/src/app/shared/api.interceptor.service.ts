import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  
  refreshTokenInProgress = false;
  
  constructor() { }
  
  /*
      Interceptor function to add Python API Token
      Pankaj Gupta
  */
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));

    console.log('Calling Intercept method...')
    
    if (currentUser && currentUser.token) {
        req = req.clone({
          setHeaders: {
            Authorization: `Token ${currentUser.accessToken}`
          }
        });
    }
    
    return next.handle(req);
  }
}
