import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamListComponent } from 'src/teams/team-list/team-list.component';
import { PlayerListComponent } from 'src/players/player-list/player-list.component';
import { FixtureListComponent } from 'src/fixtures/fixture-list/fixture-list.component';
import { TeamDetailComponent } from 'src/teams/team-detail/team-detail.component';
import { FixtureDetailComponent } from 'src/fixtures/fixture-detail/fixture-detail.component';
import { PointSummaryComponent } from 'src/teams/point-summary/point-summary.component';

const teamRoutes: Routes = [
  { 
    path: 'teams', 
    children: [
      {
        path:'',
        pathMatch: 'full',
        redirectTo:"list"
      },
      {
        path: 'list', 
        component:TeamListComponent,
      },
      {
        path: 'list/:teamId', 
        component:TeamListComponent,
      },
      {
        path: 'detail/:teamId', 
        component:TeamDetailComponent,
      },
      {
        path: 'point-summary', 
        component:PointSummaryComponent,
      }

    ]
  }
];

const playerRoutes: Routes = [
  { 
    path: 'players', 
    children: [
      {
        path:'',
        pathMatch: 'full',
        redirectTo:"list"
      },
      {
        path: 'list', 
        component:PlayerListComponent,
      }
    ]
  }
];

const fixtureRoutes: Routes = [
  { 
    path: 'fixtures', 
    children: [
      {
        path:'',
        pathMatch: 'full',
        redirectTo:"list"
      },
      {
        path: 'list', 
        component:FixtureListComponent,
      },
      {
        path: 'list/:teamId', 
        component:FixtureListComponent,
      },
      {
        path: 'detail/:fixtureId/:entityType', 
        component:FixtureDetailComponent,
      },
      {
        path: 'detail', 
        component:FixtureDetailComponent,
      },
      
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(teamRoutes),
    RouterModule.forRoot(playerRoutes),
    RouterModule.forRoot(fixtureRoutes),
  ],
  exports:[RouterModule],
  declarations: []
})

export class AppRouterModule { }
