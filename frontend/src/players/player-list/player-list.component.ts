import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../players.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent implements OnInit {

  constructor(private playerservice: PlayerService, private router: Router) { }

  playerLists: any
  ngOnInit() {
    this.getPlayers()
  }

  getPlayers() {
    this.playerservice.getPlayerList({})
      .subscribe(data => {
        this.playerLists = data;
      }, err => {
        console.log(err)
      })
  }

  getPlayerDetail(playerId){
    this.router.navigate(["fixtures/detail", playerId, 'player']);
  }
}
