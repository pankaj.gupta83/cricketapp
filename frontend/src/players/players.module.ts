import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerListComponent } from './player-list/player-list.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PlayerListComponent]
})
export class PlayersModule { }
