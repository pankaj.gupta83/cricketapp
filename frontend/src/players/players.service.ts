import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private apiservice:ApiService) { }

  getPlayerList(requestParams: any): Observable<any>{

    return this.apiservice.get('/players/', requestParams);
  }

  getPlayerDetail(playerId: any): Observable<any>{
    return this.apiservice.get('/players/?id='+playerId, playerId);
  }


}
