import { Component, OnInit } from '@angular/core';
import { TeamService } from '../team.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {
  title = 'Team List';
  constructor(private teamservice: TeamService, private router: Router) { }

  teamLists: any
  ngOnInit() {
    this.getTeams()

  }

  

  getTeams() {
    this.teamservice.getTeamList('', {})
      .subscribe(data => {
        this.teamLists = data;
      }, err => {
        console.log(err)
      })
  }

  getTeamDetail(teamId){
    this.router.navigate(["teams/detail", teamId]);
  }

  getFixtureList(teamId){
    this.router.navigate(["fixtures/list", teamId]);
  }

  
}
