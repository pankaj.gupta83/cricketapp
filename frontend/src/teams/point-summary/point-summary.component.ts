import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TeamService } from '../team.service';

@Component({
  selector: 'app-point-summary',
  templateUrl: './point-summary.component.html',
  styleUrls: ['./point-summary.component.css']
})
export class PointSummaryComponent implements OnInit {

  constructor(private actrouter:ActivatedRoute, private router:Router, private teamservice:TeamService) { }

  teamId:any
  pointSummaryDetails:any

  ngOnInit() {
    this.actrouter.params.subscribe(function(){
      //this.teamId = params.teamId;
      //this.getPointSummaryDetail();  
    });

    this.getPointSummaryDetail();  
  }

  getPointSummaryDetail(){
    this.teamservice.getPointSummaryDetail()
    .subscribe(data=>{
      console.log(data)
      this.pointSummaryDetails = data;
      }, err => {
        console.log(err)
    })
  }

  getFixtureList(teamId){
    this.router.navigate(["fixtures/list", teamId]);
  }


}
