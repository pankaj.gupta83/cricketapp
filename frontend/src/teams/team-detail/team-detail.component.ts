import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TeamService } from '../team.service';

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css']
})
export class TeamDetailComponent implements OnInit {

  constructor(private routeract:ActivatedRoute, private router:Router, private teamservice: TeamService) { }

  teamDetails: any;
  playerLists: any;
  teamId;
  ngOnInit() {
    this.routeract.params.subscribe(
      (params)=>{
        this.teamId = params.teamId;
        this.getTeamDetail();
        this.getTeamPlayerList();
        
      }
    );
  }

  getTeamDetail() {
    this.teamservice.getTeamList(this.teamId, {})
      .subscribe(data => {
        this.teamDetails = data[0];
        
      }, err => {
        console.log(err)
      })
  }

  getTeamPlayerList() {
    this.teamservice.getTeamPlayerList(this.teamId)
      .subscribe(data => {
        this.playerLists = data;
        
      }, err => {
        console.log(err)
      })
  }

  getPlayerDetail(playerId){
    this.router.navigate(["fixtures/detail", playerId, 'player']);
  }


}
