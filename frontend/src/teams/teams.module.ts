import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamDetailComponent } from './team-detail/team-detail.component';
import { PointSummaryComponent } from './point-summary/point-summary.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TeamListComponent, TeamDetailComponent, PointSummaryComponent]
})
export class TeamsModule { }
