import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private apiservice:ApiService) { }

  getTeamList(teamId : any, requestParams: any): Observable<any>{
    let requestUrl = '/teams/' ; 
    
    if(teamId)
      requestUrl = requestUrl + '?team_id=' + teamId

    return this.apiservice.get(requestUrl, requestParams);
  }

  /*
  getTeamDetail(teamId: any): Observable<any>{
    console.log('Calling tean detail API...')
    return this.apiservice.get('/teamplayers/?team_id='+teamId,teamId);
  }
  */

  getTeamPlayerList(teamId: any): Observable<any>{
    return this.apiservice.get('/teamplayers/?team_id='+teamId, teamId);
  }

  getPointSummaryDetail(): Observable<any>{
    return this.apiservice.get('/teampoints/');
  }

}
